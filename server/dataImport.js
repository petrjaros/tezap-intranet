const fs = require('fs')
const xml2js = require('xml2js')
const logger = require('./logger')

const DIRECTORY = 'import'

function parseHours(hours) {
  const matches = /^(.*):(.*)$/.exec(hours)

  if (!matches || matches.length !== 3) {
    throw new Error(`Neplatna hodnota casu: ${hours}`)
  }
  return parseInt(matches[1], 10) + (matches[2] / 60)
}

function dateToISOString(date) {
  return `${date.getFullYear()}-${(date.getMonth() + 1)
    .toString()
    .padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`
}

function findInScheduleByDate(schedule, date) {
  return schedule.find((scheduleItem) => scheduleItem.date === date)
}

function importEmployee(db, employee) {
  let schedule
  let absence
  try {
    const employeeNumber = parseInt(employee.hlavicka[0].osobni_cislo, 10)

    schedule = employee.rozvrh[0].uvazek.map((scheduleItem) => ({
      employeeNumber,
      date: scheduleItem.$.datum,
      hours: parseHours(scheduleItem._),
    }))
    absence = []
    let absenceCode
    let absenceFrom
    let absenceTo
    let absenceFromHours
    let absenceToHours

    if (employee.nepritomnosti[0]) {
      employee.nepritomnosti[0].nepritomnost.forEach((absenceItem) => {
        absenceCode = absenceItem.dochazka_kod[0]
        absenceFrom = absenceItem.od[0]._
        absenceFromHours = parseHours(absenceItem.od[0].$.nepritomnost)
        absenceTo = absenceItem.do[0]._
        absenceToHours = parseHours(absenceItem.do[0].$.nepritomnost)
        absence.push({
          employeeNumber,
          date: absenceFrom,
          type: absenceCode,
          hours: absenceFromHours,
        })

        if (absenceFrom !== absenceTo) {
          let date = new Date(absenceFrom)
          let dateString
          const toDate = new Date(absenceTo)
          while (date.getDate() < toDate.getDate() - 1) {
            date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1)
            dateString = dateToISOString(date)
            absence.push({
              employeeNumber,
              date: dateString,
              type: absenceCode,
              hours: findInScheduleByDate(schedule, absenceFrom).hours,
            })
          }

          absence.push({
            employeeNumber,
            date: absenceTo,
            type: absenceCode,
            hours: Math.min(absenceToHours, findInScheduleByDate(schedule, absenceTo).hours),
          })
        }
      })
    }
  } catch (error) {
    throw new Error(`Nepodporovana struktura XML souboru. (${error})`)
  }

  try {
    db.beginTransaction()

    schedule.forEach((scheduleItem) => {
      db.insertSchedule(scheduleItem.employeeNumber, scheduleItem.date, scheduleItem.hours)
    })
    absence.forEach((absenceItem) =>{
      db.insertAbsence(
        absenceItem.employeeNumber,
        absenceItem.date,
        absenceItem.type,
        absenceItem.hours
      )
    })

    db.endTransaction()
  } catch (error) {
    db.cancelTransaction()
    throw new Error(`Nepodarilo se zapsat do databaze: - (${error})`)
  }
}

function importFile(db, fileName) {
  const date = new Date()
  return new Promise((resolve, reject) => {
    logger.info(`import souboru (${fileName})`)
    fs.readFile(fileName, (error, data) => {
      if (error) {
        logger.error(`Nepodarilo se otevrit soubor: ${fileName} - (${error})`)
        reject()
        return
      }
      xml2js.parseString(
        data,
        {
          tagNameProcessors: [xml2js.processors.stripPrefix],
        },
        (err, result) => {
          if (err) {
            logger.error(`Neplatny XML soubor: ${fileName} - (${error})`)
            reject()
            return
          }

          try {
            let dataWiped = false

            result.dochazky_zamestnancu.dochazka_zamestnance.forEach((employee) => {
              if (!dataWiped) {
                const month = employee.hlavicka[0].mesic
                const year = employee.hlavicka[0].rok

                db.deleteFromScheduleByMonth(year, month)
                db.deleteFromAbsenceByMonth(year, month)
                dataWiped = true
              }

              importEmployee(db, employee)
            })
            const duration = (Date.now() - date) / 1000
            logger.info(`OK (${duration}s)`)
            resolve()
          } catch (employeeError) {
            logger.info(`${employeeError}`)
            reject()
          }
        }
      )
    })
  })
}

function initialImport(db) {
  fs.readdir(DIRECTORY, async (error, files) => {
    if (error) {
      logger.error(`Nepodarilo se nacist slozku import - (${error})`)
      return
    }
    for (const file of files) {
      const filePath = `${DIRECTORY}/${file}`
      if (Date.now() - fs.statSync(filePath).mtimeMs < 30 * 24 * 60 * 60 * 1000) {
        try {
          await importFile(db, filePath)
        } catch (e) {
          logger.error(`Soubor "${fileName}" nebyl naimportován.`)
        }
      }
    }
  })
}

function watch(db) {
  let files = []
  let timeoutRes = null
  fs.watch(DIRECTORY, (eventType, fileName) => {
    if (files.indexOf(fileName) === -1) {
      files.push(fileName)
    }

    if (!timeoutRes) {
      timeoutRes = setTimeout(async () => {
        for (const file of files) {
          try {
            await importFile(db, `${DIRECTORY}/${file}`)
          } catch (e) {
            logger.error(`Soubor "${fileName}" nebyl naimportován.`)
          }
        }
        files = []
        timeoutRes = null
      }, 1000)
    }
  })
}

function dataImport(db) {
  initialImport(db)
  watch(db)
}

module.exports = dataImport
