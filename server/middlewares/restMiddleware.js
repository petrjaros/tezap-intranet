const bodyParser = require('body-parser')
const logger = require('../logger')
const Database = require('../database')

function getWorkLogs(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const rows = db.getWorkLogs()
    result.json(rows)
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function postWorkLogs(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const id = db.createWorkLog(request.body)
    result.json({
      id,
    })
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function putWorkLog(request, result, next) {
  const db = new Database()
  db.open()

  try {
    db.updateWorkLog(request.params.id, request.body)
    result.json({})
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function deleteWorkLog(request, result, next) {
  const db = new Database()
  db.open()

  try {
    db.deleteWorkLog(request.params.id)
    result.json({})
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function getSchedule(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const rows = db.getSchedule()
    result.json(rows)
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function getAbsence(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const rows = db.getAbsence()
    result.json(rows)
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function getEmployees(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const rows = db.getEmployees()
    result.json(rows)
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function postEmployee(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const id = db.createEmployee(request.body)
    result.json({
      id,
    })
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function putEmployee(request, result, next) {
  const db = new Database()
  db.open()

  try {
    db.updateEmployee(request.params.id, request.body)
    result.json({})
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function deleteEmployee(request, result, next) {
  const db = new Database()
  db.open()

  try {
    db.deleteEmployee(request.params.id)
    result.json({})
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function getWorkAccounts(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const rows = db.getWorkAccounts()
    result.json(rows)
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function postWorkAccount(request, result, next) {
  const db = new Database()
  db.open()

  try {
    const id = db.createWorkAccount(request.body)
    result.json({
      id,
    })
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function putWorkAccount(request, result, next) {
  const db = new Database()
  db.open()

  try {
    db.updateWorkAccount(request.params.id, request.body)
    result.json({})
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

function deleteWorkAccount(request, result, next) {
  const db = new Database()
  db.open()

  try {
    db.deleteWorkAccount(request.params.id)
    result.json({})
  } catch (err) {
    logger.error(err)
    next(err)
  }
  db.close()
}

module.exports = (app) => {
  app.use(bodyParser.json())
  app.get('/worklog', getWorkLogs)
  app.post('/worklog', postWorkLogs)
  app.put('/worklog/:id', putWorkLog)
  app.delete('/worklog/:id', deleteWorkLog)
  app.get('/schedule', getSchedule)
  app.get('/absence', getAbsence)
  app.get('/employee', getEmployees)
  app.post('/employee', postEmployee)
  app.put('/employee/:id', putEmployee)
  app.delete('/employee/:id', deleteEmployee)
  app.get('/work-account', getWorkAccounts)
  app.post('/work-account', postWorkAccount)
  app.put('/work-account/:id', putWorkAccount)
  app.delete('/work-account/:id', deleteWorkAccount)
}
