const enableWs = require('express-ws')
const logger = require('../logger')
const Database = require('../database')

function webSocket(ws) {
  logger.info('WebSocket has been connected')
  const db = new Database()
  db.open()

  ws.on('message', (msg) => {
    ws.send(msg)

    try {
      const rows = db.getWorkLogs()
      ws.send(JSON.stringify(rows))
    } catch (err) {
      logger.error(err)
    }
  })

  ws.on('close', () => {
    logger.info('WebSocket was closed')
  })
}

module.exports = (app) => {
  enableWs(app)
  app.ws('/ws', webSocket)
}
