const SqliteDatabase = require('better-sqlite3')
const resolvePath = require('path').resolve

class Database {
  open() {
    this.db = new SqliteDatabase(resolvePath(process.cwd(), 'data/tezap.sqlite'))
  }

  close() {
    this.db.close()
    this.selectWorkLogsStatement = undefined
    this.insertWorkLogStatement = undefined
    this.updateWorkLogStatement = undefined
    this.deleteWorkLogStatement = undefined
    this.selectScheduleStatement = undefined
    this.deleteScheduleByMonthStatement = undefined
    this.insertScheduleStatement = undefined
    this.deleteFromAbsenceByMonthStatement = undefined
    this.selectAbsenceStatement = undefined
    this.insertAbsenceStatement = undefined
    this.selectEmployeesStatement = undefined
    this.insertEmployeeStatement = undefined
    this.updateEmployeeStatement = undefined
    this.deleteEmployeeStatement = undefined
    this.selectWorkAccountsStatement = undefined
    this.insertWorkAccountStatement = undefined
    this.updateWorkAccountStatement = undefined
    this.deleteWorkAccountStatement = undefined
  }

  beginTransaction() {
    this.db.prepare('BEGIN TRANSACTION').run()
  }

  endTransaction() {
    this.db.prepare('END TRANSACTION').run()
  }

  cancelTransaction() {
    this.db.prepare('ROLLBACK TRANSACTION').run()
  }

  getWorkLogs() {
    if (!this.selectWorkLogsStatement) {
      this.selectWorkLogsStatement = this.db.prepare("SELECT id, date, employee, account, hours FROM work_logs WHERE date > date('now', '-1 year')")
    }
    return this.selectWorkLogsStatement.all()
  }

  createWorkLog(workLog) {
    if (!this.insertWorkLogStatement) {
      this.insertWorkLogStatement = this.db.prepare('INSERT INTO work_logs (date, employee, account, hours) VALUES ($date, $employee, $account, $hours)')
    }
    const info = this.insertWorkLogStatement.run({
      date: workLog.date,
      employee: workLog.employee,
      account: workLog.account,
      hours: workLog.hours,
    })

    return info.lastInsertROWID
  }

  updateWorkLog(id, workLog) {
    if (!this.updateWorkLogStatement) {
      this.updateWorkLogStatement = this.db.prepare('UPDATE work_logs SET account = $account, hours = $hours WHERE id = $id')
    }

    this.updateWorkLogStatement.run({
      id,
      account: workLog.account,
      hours: workLog.hours,
    })
  }

  deleteWorkLog(id) {
    if (!this.deleteWorkLogStatement) {
      this.deleteWorkLogStatement = this.db.prepare('DELETE FROM work_logs WHERE id = $id')
    }

    this.deleteWorkLogStatement.run({
      id,
    })
  }

  getSchedule() {
    if (!this.selectScheduleStatement) {
      this.selectScheduleStatement = this.db.prepare('SELECT id, employee, date, hours FROM schedule')
    }

    return this.selectScheduleStatement.all()
  }

  deleteFromScheduleByMonth(year, month) {
    if (!this.deleteScheduleByMonthStatement) {
      this.deleteScheduleByMonthStatement = this.db.prepare('DELETE FROM schedule WHERE date LIKE $date')
    }

    this.deleteScheduleByMonthStatement.run({
      date: `${year}-${month.toString().padStart(2, '0')}-%`,
    })
  }

  insertSchedule(employeeNumber, date, hours) {
    if (!this.insertScheduleStatement) {
      this.insertScheduleStatement = this.db.prepare('INSERT INTO schedule (employee, date, hours) SELECT id, $date, $hours FROM employees WHERE number = $employeeNumber')
    }

    this.insertScheduleStatement.run({
      employeeNumber,
      date,
      hours,
    })
  }

  deleteFromAbsenceByMonth(year, month) {
    if (!this.deleteFromAbsenceByMonthStatement) {
      this.deleteFromAbsenceByMonthStatement = this.db.prepare('DELETE FROM absence WHERE date LIKE $date')
    }

    this.deleteFromAbsenceByMonthStatement.run({
      date: `${year}-${month.toString().padStart(2, '0')}-%`,
    })
  }

  getAbsence() {
    if (!this.selectAbsenceStatement) {
      this.selectAbsenceStatement = this.db.prepare('SELECT id, employee, date, type, hours FROM absence')
    }

    return this.selectAbsenceStatement.all()
  }

  insertAbsence(employeeNumber, date, type, hours) {
    if (!this.insertAbsenceStatement) {
      this.insertAbsenceStatement = this.db.prepare('INSERT INTO absence (employee, date, type, hours) SELECT id, $date, $type, $hours FROM employees WHERE number = $employeeNumber')
    }

    this.insertAbsenceStatement.run({
      employeeNumber,
      date,
      type,
      hours,
    })
  }

  getEmployees() {
    if (!this.selectEmployeesStatement) {
      this.selectEmployeesStatement = this.db.prepare('SELECT id, number, first_name AS firstName, last_name AS lastName, working, only_work_account AS onlyWorkAccount FROM employees')
    }

    return this.selectEmployeesStatement.all()
  }

  createEmployee(employee) {
    if (!this.insertEmployeeStatement) {
      this.insertEmployeeStatement = this.db.prepare('INSERT INTO employees (number, first_name, last_name, working, only_work_account) VALUES ($number, $firstName, $lastName, $working, $onlyWorkAccount)')
    }

    const info = this.insertEmployeeStatement.run({
      number: employee.number,
      firstName: employee.firstName,
      lastName: employee.lastName,
      working: employee.working,
      onlyWorkAccount: employee.onlyWorkAccount,
    })

    return info.lastInsertROWID
  }

  updateEmployee(id, employee) {
    if (!this.updateEmployeeStatement) {
      this.updateEmployeeStatement = this.db.prepare('UPDATE employees SET number = $number, first_name = $firstName, last_name = $lastName, working = $working, only_work_account = $onlyWorkAccount WHERE id = $id')
    }

    this.updateEmployeeStatement.run({
      id,
      number: employee.number,
      firstName: employee.firstName,
      lastName: employee.lastName,
      working: employee.working,
      onlyWorkAccount: employee.onlyWorkAccount,
    })
  }

  deleteEmployee(id) {
    if (!this.deleteEmployeeStatement) {
      this.deleteEmployeeStatement = this.db.prepare('DELETE FROM employees WHERE id = $id')
    }

    this.deleteEmployeeStatement.run({
      id,
    })
  }

  getWorkAccounts() {
    if (!this.selectWorkAccountsStatement) {
      this.selectWorkAccountsStatement = this.db.prepare('SELECT id, number, description FROM work_accounts')
    }

    return this.selectWorkAccountsStatement.all()
  }

  createWorkAccount(workAccount) {
    if (!this.insertWorkAccountStatement) {
      this.insertWorkAccountStatement = this.db.prepare('INSERT INTO work_accounts (number, description) VALUES ($number, $description)')
    }

    const info = this.insertWorkAccountStatement.run({
      number: workAccount.number,
      description: workAccount.description,
    })

    return info.lastInsertROWID
  }

  updateWorkAccount(id, workAccount) {
    if (!this.updateWorkAccountStatement) {
      this.updateWorkAccountStatement = this.db.prepare('UPDATE work_accounts SET number = $number, description = $description WHERE id = $id')
    }

    this.updateWorkAccountStatement.run({
      id,
      number: workAccount.number,
      description: workAccount.description,
    })
  }

  deleteWorkAccount(id) {
    if (!this.deleteWorkAccountStatement) {
      this.deleteWorkAccountStatement = this.db.prepare('DELETE FROM work_accounts WHERE id = $id')
    }

    this.deleteWorkAccountStatement.run({
      id,
    })
  }

  upgrade() {
    this.db.prepare("DELETE FROM work_logs WHERE employee=0 OR date='NaN-NaN-NaN'").run()
  }
}

module.exports = Database
