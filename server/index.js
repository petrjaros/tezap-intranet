/* eslint consistent-return:0 */

const express = require('express')
const logger = require('./logger')
const Database = require('./database')

const argv = require('minimist')(process.argv.slice(2))
const websocketMiddleWare = require('./middlewares/websocketMiddleware')
const restMiddleWare = require('./middlewares/restMiddleware')
const setup = require('./middlewares/frontendMiddleware')
const resolve = require('path').resolve
const dataImport = require('./dataImport')

const app = express()
const db = new Database()
db.open()
db.upgrade()

websocketMiddleWare(app)
restMiddleWare(app)

// In production we need to pass these values in instead of relying on webpack
setup(app, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
})

dataImport(db)

// get the intended host and port number, use localhost and port 3000 if not provided
const customHost = argv.host || process.env.HOST
const host = customHost || null // Let http.Server use its default IPv6/4 host
const prettyHost = customHost || 'localhost'

const port = argv.port || process.env.PORT || 3000

// Start your app.
app.listen(port, host, (err) => {
  if (err) {
    return logger.error(err.message)
  }

  logger.appStarted(port, prettyHost)
})
