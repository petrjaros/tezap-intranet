// @flow
import * as React from 'react'
import { withStyles } from '@material-ui/core'
import MainMenu from '../components/MainMenu'

const styles = {
  root: {
    fontFamily: 'sans-serif',
    display: 'flex',
  },
  content: {
    flex: '1 1 auto',
  },
}

type Props = {|
  classes: {|
    root: string,
    content: string,
  |},
|}


export default function pageLayout(Content: React.ComponentType<any>): React.ComponentType<void> {
  function PageLayout(props: Props) {
    const { classes } = props

    return (
      <div className={classes.root}>
        <MainMenu />
        <Content className={classes.content} />
      </div>
    )
  }

  return withStyles(styles)(PageLayout)
}
