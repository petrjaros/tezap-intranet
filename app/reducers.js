// @flow

import { combineReducers } from 'redux'

import globalReducer from './containers/App/reducer'
import workLogReducer from './reducers/workLogReducer'
import scheduleReducer from './reducers/scheduleReducer'
import absenceReducer from './reducers/absenceReducer'
import workLogMonthlyReportReducer from './reducers/workLogMonthlyReportReducer'
import employeeReducer from './reducers/employeeReducer'
import workAccountReducer from './reducers/workAccountReducer'
// import languageProviderReducer from 'containers/LanguageProvider/reducer'
import type { State as WorkLogState } from './reducers/workLogReducer'
import type { State as ScheduleState } from './reducers/scheduleReducer'
import type { State as AbsenceState } from './reducers/absenceReducer'
import type { State as WorkLogMonthlyReportState } from './reducers/workLogMonthlyReportReducer'
import type { State as EmployeeState } from './reducers/employeeReducer'
import type { State as WorkAccountState } from './reducers/workAccountReducer'

export type State = {
  route: any,
  global: any,
  workLog: WorkLogState,
  schedule: ScheduleState,
  absence: AbsenceState,
  workLogMonthlyReport: WorkLogMonthlyReportState,
  employee: EmployeeState,
  workAccount: WorkAccountState,
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(/* asyncReducers */) {
  return combineReducers({
    global: globalReducer,
    workLog: workLogReducer,
    schedule: scheduleReducer,
    absence: absenceReducer,
    workLogMonthlyReport: workLogMonthlyReportReducer,
    employee: employeeReducer,
    workAccount: workAccountReducer,
    // language: languageProviderReducer,
    // ...asyncReducers,
  })
}
