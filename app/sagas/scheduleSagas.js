// @flow
import { call, put, takeLatest } from 'redux-saga/effects'
import { schedule as scheduleApi } from '../api'
import type { Action, Schedule } from '../types'

function* loadSchedule(): Generator<Action, void, Schedule> {
  let nextAction: Action
  try {
    const schedule = yield call(scheduleApi.get)
    nextAction = { type: 'FINISH_LOAD_SCHEDULE', schedule }
  } catch (e) {
    nextAction = { type: 'FINISH_LOAD_SCHEDULE', schedule: null }
  }
  yield put(nextAction)
}

function* loadLatestSchedule(): Generator<Action, void, void> {
  yield takeLatest('START_LOAD_SCHEDULE', loadSchedule)
}

export default [loadLatestSchedule]
