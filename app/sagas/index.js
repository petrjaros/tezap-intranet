// @flow
import workLogSagas from './workLogSagas'
import scheduleSagas from './scheduleSagas'
import absenceSagas from './absenceSagas'
import employeeSagas from './employeeSagas'
import workAccountSagas from './workAccountSagas'

export default [
  ...workLogSagas,
  ...scheduleSagas,
  ...absenceSagas,
  ...employeeSagas,
  ...workAccountSagas,
]
