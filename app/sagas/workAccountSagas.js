// @flow
import {
  call,
  put,
  takeLatest,
  takeEvery,
  take,
  actionChannel,
  select,
  throttle,
} from 'redux-saga/effects'
import { workAccounts as workAccountsApi, workAccount as workAccountApi } from '../api'
import type { Action, State, WorkAccounts, WorkAccount } from '../types'
import type {
  StartUpdateWorkAccountAction,
  StartCreateWorkAccountAction,
  StartRemoveWorkAccountAction,
} from '../actions/workAccountActionTypes'
import { getWorkAccountSelector } from '../selectors/workAccountSelectors'

function* loadWorkAccounts(): Generator<Action, void, WorkAccounts> {
  let nextAction: Action
  try {
    const workAccounts = yield call(workAccountsApi.get)
    nextAction = { type: 'FINISH_LOAD_WORK_ACCOUNTS', workAccounts }
  } catch (e) {
    nextAction = { type: 'FINISH_LOAD_WORK_ACCOUNTS', workAccounts: null }
  }
  yield put(nextAction)
}

function* loadLatestWorkAccounts(): Generator<Action, void, void> {
  yield takeLatest('START_LOAD_WORK_ACCOUNTS', loadWorkAccounts)
}

function* updateWorkAccount(action: StartUpdateWorkAccountAction): Generator<Action, void, void> {
  let nextAction: Action
  try {
    yield call(workAccountApi.put, { id: action.workAccount.id }, action.workAccount)
    nextAction = { type: 'FINISH_UPDATE_WORK_ACCOUNT', clientId: action.clientId, success: true }
  } catch (e) {
    nextAction = { type: 'FINISH_UPDATE_WORK_ACCOUNT', clientId: action.clientId, success: false }
  }
  yield put(nextAction)
}

function* updateLatestWorkAccount(): Generator<Action, void, void> {
  yield throttle(500, 'START_UPDATE_WORK_ACCOUNT', updateWorkAccount)
}

function* createWorkAccount(
  action: StartCreateWorkAccountAction
): Generator<Action, void, { id: number }> {
  let nextAction: Action
  try {
    const result = yield call(workAccountsApi.post, {}, action.workAccount)
    nextAction = {
      type: 'FINISH_CREATE_WORK_ACCOUNT',
      clientId: action.clientId,
      serverId: result.id,
      workAccount: action.workAccount,
    }
  } catch (e) {
    nextAction = {
      type: 'FINISH_CREATE_WORK_ACCOUNT',
      clientId: action.clientId,
      serverId: null,
      workAccount: action.workAccount,
    }
  }
  yield put(nextAction)
}

function* createFirstWorkAccount(): Generator<Action, void, any> {
  const channel = yield actionChannel('START_CREATE_WORK_ACCOUNT')
  while (true) {
    const action: StartCreateWorkAccountAction = yield take(channel)
    const workAccount: WorkAccount = yield select((state: State) =>
      getWorkAccountSelector(state, action.clientId)
    )
    if (!workAccount.id) {
      yield call(createWorkAccount, action)
    } else {
      const updateAction: StartUpdateWorkAccountAction = {
        type: 'START_UPDATE_WORK_ACCOUNT',
        clientId: action.clientId,
        workAccount: {
          ...action.workAccount,
          id: workAccount.id,
        },
      }
      yield put(updateAction)
    }
  }
}

function* deleteWorkAccount(action: StartRemoveWorkAccountAction): Generator<Action, void, void> {
  let nextAction: Action
  try {
    yield call(workAccountApi.delete, { id: action.workAccount.id })
    nextAction = { type: 'FINISH_REMOVE_WORK_ACCOUNT', clientId: action.clientId, success: true }
  } catch (e) {
    nextAction = { type: 'FINISH_REMOVE_WORK_ACCOUNT', clientId: action.clientId, success: false }
  }
  yield put(nextAction)
}

function* deleteEveryWorkAccount(): Generator<Action, void, void> {
  yield takeEvery('START_REMOVE_WORK_ACCOUNT', deleteWorkAccount)
}

export default [
  loadLatestWorkAccounts,
  updateLatestWorkAccount,
  createFirstWorkAccount,
  deleteEveryWorkAccount,
]
