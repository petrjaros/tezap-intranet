// @flow
import { call, put, takeLatest } from 'redux-saga/effects'
import { absence as absenceApi } from '../api'
import type { Action, Absence } from '../types'

function* loadAbsence(): Generator<Action, void, Absence> {
  let nextAction: Action
  try {
    const absence = yield call(absenceApi.get)
    nextAction = { type: 'FINISH_LOAD_ABSENCE', absence }
  } catch (e) {
    nextAction = { type: 'FINISH_LOAD_ABSENCE', absence: null }
  }
  yield put(nextAction)
}

function* loadLatestAbsence(): Generator<Action, void, void> {
  yield takeLatest('START_LOAD_ABSENCE', loadAbsence)
}

export default [loadLatestAbsence]
