// @flow
import { call, put, takeLatest, takeEvery, actionChannel, take, select } from 'redux-saga/effects'
import { workLogs as workLogsApi, workLog as workLogApi } from '../api'
import { getWorkLogItemSelector } from '../selectors/workLogSelectors'
import type { State, Action, WorkLog, WorkLogItem } from '../types'
import type {
  StartUpdateWorkLogAction,
  StartCreateWorkLogAction,
  StartRemoveWorkLogAction,
} from '../actions/workLogActionTypes'

function* loadWorkLogs(): Generator<Action, void, WorkLog> {
  let nextAction: Action
  try {
    const workLogs = yield call(workLogsApi.get)
    nextAction = { type: 'FINISH_LOAD_WORK_LOGS', workLogs }
  } catch (e) {
    nextAction = { type: 'FINISH_LOAD_WORK_LOGS', workLogs: null }
  }
  yield put(nextAction)
}

function* loadLatestWorkLogs(): Generator<Action, void, void> {
  yield takeLatest('START_LOAD_WORK_LOGS', loadWorkLogs)
}

function* updateWorkLog(action: StartUpdateWorkLogAction): Generator<Action, void, void> {
  let nextAction: Action
  try {
    yield call(workLogApi.put, { id: action.workLog.id }, action.workLog)
    nextAction = { type: 'FINISH_UPDATE_WORK_LOG', clientId: action.clientId, success: true }
  } catch (e) {
    nextAction = { type: 'FINISH_UPDATE_WORK_LOG', clientId: action.clientId, success: false }
  }
  yield put(nextAction)
}

function* updateEveryWorkLog(): Generator<Action, void, void> {
  yield takeEvery('START_UPDATE_WORK_LOG', updateWorkLog)
}

function* createWorkLog(action: StartCreateWorkLogAction): Generator<Action, void, { id: number }> {
  let nextAction: Action
  try {
    const result = yield call(workLogsApi.post, {}, action.workLog)
    nextAction = {
      type: 'FINISH_CREATE_WORK_LOG',
      clientId: action.clientId,
      serverId: result.id,
      workLog: action.workLog,
    }
  } catch (e) {
    nextAction = {
      type: 'FINISH_CREATE_WORK_LOG',
      clientId: action.clientId,
      serverId: null,
      workLog: action.workLog,
    }
  }
  yield put(nextAction)
}

function* createFirstWorkLog(): Generator<Action, void, any> {
  const channel = yield actionChannel('START_CREATE_WORK_LOG')
  while (true) {
    const action: StartCreateWorkLogAction = yield take(channel)
    const workLog: WorkLogItem = yield select((state: State) =>
      getWorkLogItemSelector(state, action.clientId)
    )
    if (!workLog.id) {
      yield call(createWorkLog, action)
    } else {
      const updateAction: StartUpdateWorkLogAction = {
        type: 'START_UPDATE_WORK_LOG',
        clientId: action.clientId,
        workLog: {
          ...action.workLog,
          id: workLog.id,
        },
      }
      yield put(updateAction)
    }
  }
}

function* deleteWorkLog(action: StartRemoveWorkLogAction): Generator<Action, void, void> {
  let nextAction: Action
  try {
    yield call(workLogApi.delete, { id: action.workLog.id })
    nextAction = { type: 'FINISH_REMOVE_WORK_LOG', clientId: action.clientId, success: true }
  } catch (e) {
    nextAction = { type: 'FINISH_REMOVE_WORK_LOG', clientId: action.clientId, success: false }
  }
  yield put(nextAction)
}

function* deleteEveryWorkLog(): Generator<Action, void, void> {
  yield takeEvery('START_REMOVE_WORK_LOG', deleteWorkLog)
}

export default [loadLatestWorkLogs, updateEveryWorkLog, createFirstWorkLog, deleteEveryWorkLog]
