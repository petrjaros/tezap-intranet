// @flow
import {
  call,
  put,
  takeLatest,
  takeEvery,
  take,
  actionChannel,
  select,
  throttle,
} from 'redux-saga/effects'
import { employees as employeesApi, employee as employeeApi } from '../api'
import type { Action, State, Employees, Employee } from '../types'
import type {
  StartUpdateEmployeeAction,
  StartCreateEmployeeAction,
  StartRemoveEmployeeAction,
} from '../actions/employeeActionTypes'
import { getEmployeeSelector } from '../selectors/employeeSelectors'

function* loadEmployees(): Generator<Action, void, Employees> {
  let nextAction: Action
  try {
    const employees = yield call(employeesApi.get)
    nextAction = { type: 'FINISH_LOAD_EMPLOYEES', employees }
  } catch (e) {
    nextAction = { type: 'FINISH_LOAD_EMPLOYEES', employees: null }
  }
  yield put(nextAction)
}

function* loadLatestEmployees(): Generator<Action, void, void> {
  yield takeLatest('START_LOAD_EMPLOYEES', loadEmployees)
}

function* updateEmployee(action: StartUpdateEmployeeAction): Generator<Action, void, void> {
  let nextAction: Action
  try {
    yield call(employeeApi.put, { id: action.employee.id }, action.employee)
    nextAction = { type: 'FINISH_UPDATE_EMPLOYEE', clientId: action.clientId, success: true }
  } catch (e) {
    nextAction = { type: 'FINISH_UPDATE_EMPLOYEE', clientId: action.clientId, success: false }
  }
  yield put(nextAction)
}

function* updateLatestEmployee(): Generator<Action, void, void> {
  yield throttle(500, 'START_UPDATE_EMPLOYEE', updateEmployee)
}

function* createEmployee(
  action: StartCreateEmployeeAction
): Generator<Action, void, { id: number }> {
  let nextAction: Action
  try {
    const result = yield call(employeesApi.post, {}, action.employee)
    nextAction = {
      type: 'FINISH_CREATE_EMPLOYEE',
      clientId: action.clientId,
      serverId: result.id,
      employee: action.employee,
    }
  } catch (e) {
    nextAction = {
      type: 'FINISH_CREATE_EMPLOYEE',
      clientId: action.clientId,
      serverId: null,
      employee: action.employee,
    }
  }
  yield put(nextAction)
}

function* createFirstEmployee(): Generator<Action, void, any> {
  const channel = yield actionChannel('START_CREATE_EMPLOYEE')
  while (true) {
    const action: StartCreateEmployeeAction = yield take(channel)
    const employee: Employee = yield select((state: State) =>
      getEmployeeSelector(state, action.clientId)
    )
    if (!employee.id) {
      yield call(createEmployee, action)
    } else {
      const updateAction: StartUpdateEmployeeAction = {
        type: 'START_UPDATE_EMPLOYEE',
        clientId: action.clientId,
        employee: {
          ...action.employee,
          id: employee.id,
        },
      }
      yield put(updateAction)
    }
  }
}

function* deleteEmployee(action: StartRemoveEmployeeAction): Generator<Action, void, void> {
  let nextAction: Action
  try {
    yield call(employeeApi.delete, { id: action.employee.id })
    nextAction = { type: 'FINISH_REMOVE_EMPLOYEE', clientId: action.clientId, success: true }
  } catch (e) {
    nextAction = { type: 'FINISH_REMOVE_EMPLOYEE', clientId: action.clientId, success: false }
  }
  yield put(nextAction)
}

function* deleteEveryEmployee(): Generator<Action, void, void> {
  yield takeEvery('START_REMOVE_EMPLOYEE', deleteEmployee)
}

export default [loadLatestEmployees, updateLatestEmployee, createFirstEmployee, deleteEveryEmployee]
