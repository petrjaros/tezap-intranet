// @flow
import { createSelector } from 'reselect'
import { toISODateString } from '../utils/formatters'
import type { State } from '../types'
import type { WorkLogState } from '../reducers/workLogReducer'

const getWorkLogStates = (state: State) => state.workLog.data || []
const getCurrentWorkLogDate = (state: State) => toISODateString(state.workLog.date)
const getCurrentWorkLogEmployee = (state: State) => state.workLog.employee

export const getCurrentWorkLogSelector = createSelector(
  getWorkLogStates,
  getCurrentWorkLogDate,
  getCurrentWorkLogEmployee,
  (workLogStates: Array<WorkLogState>, date: Date, employeeId: number) =>
    workLogStates.filter(
      (workLogState: WorkLogState) =>
        workLogState.data.date === date && workLogState.data.employee === employeeId
    )
)

export const getWorkLogStateSelector = createSelector(
  getWorkLogStates,
  (state: State, clientId: number) => clientId,
  (workLogStates: Array<WorkLogState>, clientId: number) =>
    workLogStates.find((workLogState: WorkLogState) => workLogState.clientId === clientId)
)

export const getWorkLogItemSelector = createSelector(
  getWorkLogStateSelector,
  (workLogState: WorkLogState) => workLogState.data
)
