// @flow
import { createSelector } from 'reselect'
import type { State, Employee } from '../types'
import type { EmployeeState } from '../reducers/employeeReducer'

const getEmployeeStates = (state: State) => state.employee.data || []

export const getEmployeesSelector = createSelector(
  getEmployeeStates,
  (employeeStates: Array<EmployeeState>) =>
    employeeStates.map((employeeState: EmployeeState) => employeeState.data)
)

export const getAvailableEmployeesSelector = createSelector(
  getEmployeesSelector,
  (employees: Array<Employee>) =>
    employees.filter((employee: Employee) => !employee.onlyWorkAccount)
)

export const getEmployeeStateSelector = createSelector(
  getEmployeeStates,
  (state: State, clientId: number) => clientId,
  (employeeStates: Array<EmployeeState>, clientId: number) =>
    employeeStates.find((employeeState: EmployeeState) => employeeState.clientId === clientId)
)

export const getEmployeeSelector = createSelector(
  getEmployeeStateSelector,
  (employeeState: EmployeeState) => employeeState.data
)
