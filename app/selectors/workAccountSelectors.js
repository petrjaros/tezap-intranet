// @flow
import { createSelector } from 'reselect'
import type { State } from '../types'
import type { WorkAccountState } from '../reducers/workAccountReducer'

const getWorkAccountStates = (state: State) => state.workAccount.data || []

export const getWorkAccountsSelector = createSelector(
  getWorkAccountStates,
  (workAccountStates: Array<WorkAccountState>) =>
    workAccountStates.map((workAccountState: WorkAccountState) => workAccountState.data)
)

export const getWorkAccountStateSelector = createSelector(
  getWorkAccountStates,
  (state: State, clientId: number) => clientId,
  (workAccountStates: Array<WorkAccountState>, clientId: number) =>
    workAccountStates.find(
      (workAccountState: WorkAccountState) => workAccountState.clientId === clientId
    )
)

export const getWorkAccountSelector = createSelector(
  getWorkAccountStateSelector,
  (workAccountState: WorkAccountState) => workAccountState.data
)
