// @flow
export function generateId(): number {
  return Math.round(Math.random() * Number.MAX_SAFE_INTEGER)
}
