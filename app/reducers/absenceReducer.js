// @flow
import type { Action, AbsenceItem } from '../types'

export type State = {
  loading: boolean,
  data: { [number]: { [string]: Array<AbsenceItem> } },
}

const initialState = {
  loading: false,
  data: {},
}

function absenceReducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case 'START_LOAD_ABSENCE':
      return { ...state, loading: true }
    case 'FINISH_LOAD_ABSENCE': {
      const newState = { ...state, loading: false }

      if (action.absence) {
        newState.data = {}
        action.absence.forEach((absenceItem: AbsenceItem) => {
          newState.data[absenceItem.employee] = newState.data[absenceItem.employee] || {}
          newState.data[absenceItem.employee][absenceItem.date] =
            newState.data[absenceItem.employee][absenceItem.date] || []
          newState.data[absenceItem.employee][absenceItem.date].push(absenceItem)
        })
      }
      return newState
    }
    default:
      return state
  }
}

export default absenceReducer
