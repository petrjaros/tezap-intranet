// @flow
import type { Action, WorkAccount } from '../types'

export type WorkAccountState = {
  creating: boolean,
  updating: boolean,
  removing: boolean,
  clientId: number,
  data: WorkAccount,
}

export type State = {
  loading: boolean,
  loaded: boolean,
  data: Array<WorkAccountState>,
}

const initialState = {
  loading: false,
  loaded: false,
  data: [],
}

function workAccountReducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case 'START_LOAD_WORK_ACCOUNTS':
      return { ...state, loading: true }
    case 'FINISH_LOAD_WORK_ACCOUNTS': {
      let workAccounts: Array<WorkAccountState> = state.data
      let loaded = false

      if (action.workAccounts) {
        loaded = true
        workAccounts = []
        action.workAccounts.forEach((workAccount: WorkAccount) => {
          workAccounts.push({
            creating: false,
            updating: false,
            removing: false,
            clientId: workAccount.id || 0,
            data: workAccount,
          })
        })
      }
      return { ...state, loading: false, loaded, data: workAccounts }
    }
    case 'START_CREATE_WORK_ACCOUNT': {
      const clientId = action.clientId
      const newData = [...state.data]
      const index = newData.findIndex(
        (workAccountState: WorkAccountState) => workAccountState.clientId === clientId
      )
      const newWorkAccount = {
        creating: true,
        updating: false,
        removing: false,
        clientId: action.clientId,
        data: action.workAccount,
      }
      if (index === -1) {
        newData.push(newWorkAccount)
      } else {
        newData[index] = newWorkAccount
      }
      return { ...state, data: newData }
    }
    case 'FINISH_CREATE_WORK_ACCOUNT': {
      const clientId = action.clientId
      const newData = [...state.data]
      const index = newData.findIndex(
        (workAccountState: WorkAccountState) => workAccountState.clientId === clientId
      )
      const workAccountState: ?WorkAccountState = newData[index]

      if (workAccountState) {
        newData[index] = {
          ...workAccountState,
          creating: false,
          data: {
            id: action.serverId,
            number: workAccountState.data.number,
            description: workAccountState.data.description,
          },
        }
      }
      return { ...state, data: newData }
    }
    case 'START_UPDATE_WORK_ACCOUNT': {
      const clientId = action.clientId
      const newData = [...state.data]
      const index = newData.findIndex(
        (workAccountState: WorkAccountState) => workAccountState.clientId === clientId
      )
      const workAccountState: ?WorkAccountState = newData[index]

      if (workAccountState) {
        newData[index] = {
          ...workAccountState,
          updating: true,
          data: {
            id: workAccountState.data.id,
            number: action.workAccount.number,
            description: action.workAccount.description,
          },
        }
      }
      return { ...state, data: newData }
    }
    case 'START_REMOVE_WORK_ACCOUNT': {
      const clientId = action.clientId
      const newData = [...state.data]
      const index = newData.findIndex(
        (workAccountState: WorkAccountState) => workAccountState.clientId === clientId
      )
      const workAccountState: ?WorkAccountState = newData[index]

      if (workAccountState) {
        newData.splice(index, 1)
      }
      return { ...state, data: newData }
    }
    default:
      return state
  }
}

export default workAccountReducer
