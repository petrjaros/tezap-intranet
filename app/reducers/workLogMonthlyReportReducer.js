// @flow
import type { Action } from '../types'

export type State = {
  month: number,
  year: number,
}

const now = new Date()
const initialState = {
  month: now.getMonth(),
  year: now.getFullYear(),
}

function workLogMonthlyReportReducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case 'CHANGE_MONTHLY_REPORT_MONTH':
      return { ...state, month: action.month, year: action.year }
    default:
      return state
  }
}

export default workLogMonthlyReportReducer
