// @flow
import { isEqual } from 'lodash'
import { merge, replaceAt } from 'timm'
import type { Action, WorkLogItem, Employee } from '../types'

export type WorkLogState = {
  creating: boolean,
  updating: boolean,
  removing: boolean,
  clientId: number,
  data: WorkLogItem,
}

export type State = {
  employee: number,
  date: Date,
  loading: boolean,
  data: Array<WorkLogState>,
}

const initialState = {
  employee: 0,
  date: new Date(),
  loading: false,
  data: [],
}

function workLogReducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case 'FINISH_LOAD_EMPLOYEES': {
      if (action.employees) {
        const workingEmployees = action.employees.filter(
          (employee: Employee) => Boolean(employee.working)
        )
        const employeeId = workingEmployees[0].id
        if (employeeId) {
          return state.employee === 0 && workingEmployees.length > 0
            ? { ...state, employee: employeeId } : state
        }
      }
      return state
    }
    case 'CHANGE_EMPLOYEE':
      return { ...state, employee: action.employee }
    case 'CHANGE_DATE':
      return { ...state, date: action.date }
    case 'START_LOAD_WORK_LOGS':
      return { ...state, loading: true }
    case 'FINISH_LOAD_WORK_LOGS': {
      let newWorkLogStates = state.data

      if (action.workLogs) {
        const { workLogs: actionWorkLogs } = action
        actionWorkLogs.forEach((workLogItem: WorkLogItem, index: number) => {
          const newWorkLogState = {
            creating: false,
            updating: false,
            removing: false,
            clientId: workLogItem.id || 0,
            data: workLogItem,
          }
          if (newWorkLogStates[index]) {
            newWorkLogState.data = isEqual(workLogItem, newWorkLogStates[index].data)
              ? newWorkLogStates[index].data
              : workLogItem
            newWorkLogStates = replaceAt(newWorkLogStates, index, newWorkLogState)
          } else {
            if (newWorkLogStates === state.data) {
              newWorkLogStates = [...newWorkLogStates]
            }
            newWorkLogStates.push(newWorkLogState)
          }
          if (actionWorkLogs.length < newWorkLogStates.length) {
            newWorkLogStates = newWorkLogStates.slice(0, actionWorkLogs.length)
          }
        })
      }

      return merge(state, { loading: false, data: newWorkLogStates })
    }
    case 'START_CREATE_WORK_LOG': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (workLogState: WorkLogState) => workLogState.clientId === clientId
      )
      const newWorkLog = {
        creating: true,
        updating: false,
        removing: false,
        clientId: action.clientId,
        data: action.workLog,
      }
      if (index === -1) {
        newData.push(newWorkLog)
      } else {
        newData[index] = newWorkLog
      }
      return { ...state, data: newData }
    }
    case 'FINISH_CREATE_WORK_LOG': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (workLogState: WorkLogState) => workLogState.clientId === clientId
      )
      const workLogState: ?WorkLogState = newData[index]

      if (workLogState) {
        newData[index] = {
          ...workLogState,
          creating: false,
          data: {
            id: action.serverId,
            employee: workLogState.data.employee,
            date: workLogState.data.date,
            account: workLogState.data.account,
            hours: workLogState.data.hours,
          },
        }
      }
      return { ...state, data: newData }
    }
    case 'START_UPDATE_WORK_LOG': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (workLogState: WorkLogState) => workLogState.clientId === clientId
      )
      const workLogState: ?WorkLogState = newData[index]

      if (workLogState) {
        newData[index] = {
          ...workLogState,
          updating: true,
          data: {
            id: workLogState.data.id,
            employee: action.workLog.employee,
            date: action.workLog.date,
            account: action.workLog.account,
            hours: action.workLog.hours,
          },
        }
      }
      return { ...state, data: newData }
    }
    case 'START_REMOVE_WORK_LOG': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (workLogState: WorkLogState) => workLogState.clientId === clientId
      )
      const workLogState: ?WorkLogState = newData[index]

      if (workLogState) {
        newData.splice(index, 1)
      }
      return { ...state, data: newData }
    }
    default:
      return state
  }
}

export default workLogReducer
