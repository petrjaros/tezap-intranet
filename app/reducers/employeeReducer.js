// @flow
import { isEqual } from 'lodash'
import { merge, replaceAt } from 'timm'
import type { Action, Employee } from '../types'

export type EmployeeState = {
  creating: boolean,
  updating: boolean,
  removing: boolean,
  clientId: number,
  data: Employee,
}

export type State = {
  loading: boolean,
  loaded: boolean,
  data: Array<EmployeeState>,
}

const initialState = {
  loading: false,
  loaded: false,
  data: [],
}

function employeeReducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case 'START_LOAD_EMPLOYEES':
      return { ...state, loading: true }
    case 'FINISH_LOAD_EMPLOYEES': {
      const employees: Array<EmployeeState> = state.data
      const updatedEmployees = []
      let newEmployees: Array<EmployeeState> = employees
      let newState: State = state

      if (action.employees) {
        const { employees: actionEmployees } = action
        newState = merge(newState, { loaded: true })
        actionEmployees.forEach((employee: Employee, index: number) => {
          const employeeState = state.data[index]
          const newEmployeeState = {
            creating: false,
            updating: false,
            removing: false,
            clientId: employee.id || 0,
            data: employee,
          }

          if (employeeState) {
            newEmployeeState.data = isEqual(employeeState.data, employee)
              ? employeeState.data
              : employee
            newEmployees = replaceAt(
              newEmployees,
              index,
              merge(employeeState, newEmployeeState)
            )
            updatedEmployees[index] = true
          } else {
            if (newEmployees === employees) {
              newEmployees = [...employees]
            }
            newEmployees.push(newEmployeeState)
          }
        })

        if (actionEmployees.length < newEmployees.length) {
          newEmployees = newEmployees.slice(0, actionEmployees.length)
        }
      }
      return merge(newState, { loading: false, data: newEmployees })
    }
    case 'START_CREATE_EMPLOYEE': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (employeeState: EmployeeState) => employeeState.clientId === clientId
      )
      const newEmployee = {
        creating: true,
        updating: false,
        removing: false,
        clientId: action.clientId,
        data: action.employee,
      }
      if (index === -1) {
        newData.push(newEmployee)
      } else {
        newData[index] = newEmployee
      }
      return { ...state, data: newData }
    }
    case 'FINISH_CREATE_EMPLOYEE': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (employeeState: EmployeeState) => employeeState.clientId === clientId
      )
      const employeeState: ?EmployeeState = newData[index]

      if (employeeState) {
        newData[index] = {
          ...employeeState,
          creating: false,
          data: {
            id: action.serverId,
            number: employeeState.data.number,
            firstName: employeeState.data.firstName,
            lastName: employeeState.data.lastName,
            working: employeeState.data.working,
            onlyWorkAccount: employeeState.data.onlyWorkAccount,
          },
        }
      }
      return { ...state, data: newData }
    }
    case 'START_UPDATE_EMPLOYEE': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (employeeState: EmployeeState) => employeeState.clientId === clientId
      )
      const employeeState: ?EmployeeState = newData[index]

      if (employeeState) {
        newData[index] = {
          ...employeeState,
          updating: true,
          data: {
            id: employeeState.data.id,
            number: action.employee.number,
            firstName: action.employee.firstName,
            lastName: action.employee.lastName,
            working: action.employee.working,
            onlyWorkAccount: action.employee.onlyWorkAccount,
          },
        }
      }
      return { ...state, data: newData }
    }
    case 'START_REMOVE_EMPLOYEE': {
      const { clientId } = action
      const newData = [...state.data]
      const index = newData.findIndex(
        (employeeState: EmployeeState) => employeeState.clientId === clientId
      )
      const employeeState: ?EmployeeState = newData[index]

      if (employeeState) {
        newData.splice(index, 1)
      }
      return { ...state, data: newData }
    }
    default:
      return state
  }
}

export default employeeReducer
