// @flow
import type { Action, ScheduleItem } from '../types'

export type State = {
  loading: boolean,
  data: { [number]: { [string]: number } },
}

const initialState = {
  loading: false,
  data: {},
}

function scheduleReducer(state: State = initialState, action: Action): State {
  switch (action.type) {
    case 'START_LOAD_SCHEDULE':
      return { ...state, loading: true }
    case 'FINISH_LOAD_SCHEDULE': {
      const newState = { ...state, loading: false }

      if (action.schedule) {
        newState.data = {}
        action.schedule.forEach((scheduleItem: ScheduleItem) => {
          newState.data[scheduleItem.employee] = newState.data[scheduleItem.employee] || {}
          newState.data[scheduleItem.employee][scheduleItem.date] = scheduleItem.hours
        })
      }
      return newState
    }
    default:
      return state
  }
}

export default scheduleReducer
