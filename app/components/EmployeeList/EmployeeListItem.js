// @flow
import React, { Component } from 'react'
import {
  withStyles, TextField, Button, MenuItem, Checkbox, Dialog,
  DialogTitle, DialogContent, DialogContentText, DialogActions,
} from '@material-ui/core'
import type { Employee, WorkAccounts, WorkAccount } from '../../types'

function isNumberValid(number: ?number): boolean {
  if (!number) {
    return false
  }
  return number > 0
}

function isNameValid(name: string): boolean {
  return name !== ''
}

const styles = {
  numberColumn: {
    width: 50,
  },
  column: {
    width: 170,
  },
}

type Props = {|
  clientId: number,
  data: Employee,
  workAccounts: ?WorkAccounts,
  creating: boolean,
  updating: boolean,
  removing: boolean,
  classes: $ObjMap<typeof styles, () => string>,
  onChange: (number, Employee) => void,
  onRemove: (number, Employee) => void,
|}

type State = {|
  number: number,
  firstName: string,
  lastName: string,
  onlyWorkAccount: ?string,
  working: boolean,
  removeDialogOpen: boolean,
|}

class EmployeeListItem extends Component<Props, State> {
  static defaultProps = {
    data: {
      id: null,
      number: 0,
      firstName: '',
      lastName: '',
      working: 1,
      onlyWorkAccount: null,
    },
    workAccounts: [],
    creating: false,
    updating: false,
    removing: false,
    onChange: () => {},
    onRemove: () => {},
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      number: props.data.number,
      firstName: props.data.firstName,
      lastName: props.data.lastName,
      onlyWorkAccount: props.data.onlyWorkAccount,
      working: !!props.data.working,
      removeDialogOpen: false,
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    const { data } = this.props

    if (nextProps.data !== data) {
      this.setState({
        number: nextProps.data.number,
        firstName: nextProps.data.firstName,
        lastName: nextProps.data.lastName,
        onlyWorkAccount: nextProps.data.onlyWorkAccount,
      })
    }
  }

  handleRemoveClick = () => {
    this.setState({
      removeDialogOpen: true,
    })
  }

  handleNumberChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const number: number = parseFloat(event.target.value) || 0
    this.setState({
      number,
    })

    if (this.isValid(number)) {
      this.fireOnChange(number)
    }
  }

  handleFirstNameChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    if (!event.target) {
      return
    }

    this.setState({
      firstName: event.target.value,
    })

    if (this.isValid(undefined, event.target.value)) {
      this.fireOnChange(undefined, event.target.value)
    }
  }

  handleLastNameChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({
      lastName: event.target.value,
    })

    if (this.isValid(undefined, undefined, event.target.value)) {
      this.fireOnChange(undefined, undefined, event.target.value)
    }
  }

  handleOnlyWorkAccountChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({
      onlyWorkAccount: event.target.value,
    })

    if (this.isValid()) {
      this.fireOnChange(undefined, undefined, undefined, event.target.value)
    }
  }

  handleWorkingChange = (event: SyntheticInputEvent<HTMLInputElement>, checked: boolean) => {
    this.setState({
      working: checked,
    })

    if (this.isValid()) {
      this.fireOnChange(undefined, undefined, undefined, undefined, checked)
    }
  }

  handleRemove = () => {
    const { clientId, data, onRemove } = this.props
    onRemove(clientId, data)
  }

  handleDialogClose = () => {
    this.setState({
      removeDialogOpen: false,
    })
  }

  isValid(number: ?number, fistName: ?string, lastName: ?string) {
    const { number: stateNumber, firstName: stateFirstName, lastName: stateLastName } = this.state

    return (
      isNumberValid(number || stateNumber)
      && isNameValid(fistName || stateFirstName)
      && isNameValid(lastName || stateLastName)
    )
  }

  fireOnChange(
    number: number | void,
    firstName: string | void,
    lastName: string | void,
    onlyWorkAccount: ?string,
    working: boolean | void,
  ) {
    const { data, clientId, onChange } = this.props
    const {
      number: stateNumber,
      firstName: stateFirstName,
      lastName: stateLastName,
      onlyWorkAccount: stateOnlyWorkAccount,
      working: stateWorking,
    } = this.state

    onChange(clientId, {
      id: data.id,
      number: number === undefined ? stateNumber : number,
      firstName: firstName === undefined ? stateFirstName : firstName,
      lastName: lastName === undefined ? stateLastName : lastName,
      onlyWorkAccount: onlyWorkAccount === undefined ? stateOnlyWorkAccount : onlyWorkAccount,
      working: (working === undefined ? stateWorking : working) ? 1 : 0,
    })
  }

  render() {
    const { classes, workAccounts, data } = this.props
    const {
      number, firstName, lastName, onlyWorkAccount, working, removeDialogOpen,
    } = this.state

    return (
      <div>
        <TextField
          type="number"
          value={number || ''}
          onChange={this.handleNumberChange}
          placeholder="Číslo"
          className={classes.numberColumn}
        />
        <TextField
          value={firstName || ''}
          onChange={this.handleFirstNameChange}
          placeholder="Jméno"
          className={classes.column}
        />
        <TextField
          value={lastName || ''}
          onChange={this.handleLastNameChange}
          placeholder="Příjmení"
          className={classes.column}
        />
        <TextField
          select
          value={onlyWorkAccount || ''}
          onChange={this.handleOnlyWorkAccountChange}
          SelectProps={{ displayEmpty: true, renderValue: (value: string) => value || 'THP+sklad' }}
          className={classes.column}
        >
          <MenuItem value="" />
          {workAccounts && workAccounts.map((workAccount: WorkAccount) => (
            <MenuItem
              key={workAccount.id}
              value={workAccount.number}
            >
              {workAccount.number}
              {' '}
              {workAccount.description}
            </MenuItem>
          ))}
        </TextField>
        <Checkbox checked={working} onChange={this.handleWorkingChange} />
        {data.id && <Button variant="text" onClick={this.handleRemoveClick}>Smazat</Button>}
        <Dialog open={removeDialogOpen} onClose={this.handleDialogClose}>
          <DialogTitle>Opravdu smazat?</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {`Opravdu zamazat zaměstnance ${firstName} ${lastName}?`}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="secondary">
              Zavřít
            </Button>
            <Button onClick={this.handleRemove} color="primary" autoFocus>
              Smazat
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default withStyles(styles)(EmployeeListItem)
