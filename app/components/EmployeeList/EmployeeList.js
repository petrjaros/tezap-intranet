// @flow
import React, { Component } from 'react'
import EmployeeListItem from './EmployeeListItem'
import { generateId } from '../../utils'
import type { Employee, WorkAccounts } from '../../types'
import type { EmployeeState } from '../../reducers/employeeReducer'

export type Props = {|
  data: ?Array<EmployeeState>,
  workAccounts: ?WorkAccounts,
  onNeedEmployeeData: () => void,
  onNeedWorkAccountData: () => void,
  onChange: (number, Employee) => void,
  onRemove: (number, Employee) => void,
|}

class EmployeeList extends Component<Props> {
  static defaultProps = {
    onNeedEmployeeData: () => {},
    onCreate: () => {},
    onChange: () => {},
    onRemove: () => {},
  }

  componentDidMount() {
    this.props.onNeedEmployeeData()
    this.props.onNeedWorkAccountData()
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.data && nextProps.data !== this.props.data) {
      if (nextProps.data.find((item: EmployeeState) => item.clientId === this.newItemId)) {
        this.newItemId = generateId()
      }
    }
  }

  newItemId: number = generateId()

  render() {
    if (!this.props.data) {
      return null
    }

    return (
      <div>
        {[...this.props.data, null].map(
          (item: ?EmployeeState) =>
            (item ? (
              <EmployeeListItem
                key={item.clientId}
                clientId={item.clientId}
                workAccounts={this.props.workAccounts}
                creating={item.creating}
                updating={item.updating}
                removing={item.removing}
                data={item.data}
                onChange={this.props.onChange}
                onRemove={this.props.onRemove}
              />
            ) : (
              <EmployeeListItem
                key={item ? item.clientId : this.newItemId}
                clientId={this.newItemId}
                workAccounts={this.props.workAccounts}
                onChange={this.props.onChange}
                onRemove={this.props.onRemove}
              />
            ))
        )}
      </div>
    )
  }
}

export default EmployeeList
