// @flow
import React, { Component } from 'react'
import { withStyles, TextField, MenuItem } from '@material-ui/core'
import Holidays from 'date-holidays'
import type {
  WorkLogState, Employees, Employee, AbsenceItem,
} from '../../types'

export type Props = {|
  className: string,
  employees: Employees,
  month: number,
  year: number,
  workLog: Array<WorkLogState>,
  scheduleHours: { [number]: { [string]: number } },
  absence: { [number]: { [string]: Array<AbsenceItem> }},
  onNeedEmployeeData: () => void,
  onNeedWorkLogData: () => void,
  onMonthChange: (year: number, month: number) => void,
  onNeedScheduleData: () => void,
  onNeedAbsenceData: () => void,
|}

type Classes = {|
  classes: {|
    header: string,
    account: string,
  |},
|}

const styles = () => ({
  account: {
    display: 'inline-block',
  },
})

const printStyles = () => ({
  header: {
    display: 'none',
  },
})

type AccountData = { name: string, hours: number }
type ReportItem = { id: number, accounts: Array<AccountData>, employee: ?Employee, sum?: number }
type Report = Array<ReportItem>

function yearAndMonthToKey(year: number, month: number): number {
  return year * 12 + month
}

function keyToMonth(key: number): number {
  return key % 12
}

function keyToYear(key: number): number {
  return Math.floor(key / 12)
}

class WorkLogMonthlyReport extends Component<Props & Classes> {
  static defaultProps = {
    onNeedEmployeeData: () => {},
    onNeedWorkLogData: () => {},
    onNeedScheduleData: () => {},
    onNeedAbsenceData: () => {},
  }

  holidays = new Holidays('CZ')

  constructor(props: Props & Classes) {
    super(props)
    this.calculateReport(
      props.workLog, props.employees, props.scheduleHours, props.absence, props.year, props.month
    )
  }

  componentDidMount() {
    const {
      onNeedEmployeeData, onNeedWorkLogData, onNeedScheduleData, onNeedAbsenceData,
    } = this.props

    onNeedEmployeeData()
    onNeedWorkLogData()
    onNeedScheduleData()
    onNeedAbsenceData()
  }

  componentWillReceiveProps(nextProps: Props & Classes) {
    this.calculateReport(
      nextProps.workLog,
      nextProps.employees,
      nextProps.scheduleHours,
      nextProps.absence,
      nextProps.year,
      nextProps.month,
    )
  }

  onMonthSelectFieldChange = (event: SyntheticInputEvent<TextField>) => {
    const { onMonthChange } = this.props
    const { value } = event.target

    if (typeof value === 'number') {
      onMonthChange(keyToYear(value), keyToMonth(value))
    }
  }

  report: Report

  calculateReport(
    workLog: Array<WorkLogState>,
    employees: Employees,
    scheduleHours: { [number]: { [string]: number } },
    absence: { [number]: { [string]: Array<AbsenceItem> }},
    year: number,
    month: number,
  ) {
    this.report = []
    let employeeReport: ?ReportItem
    let account: ?AccountData
    let employeeId: number

    workLog
      .filter((workLogState: WorkLogState) => `${year}-${(month + 1).toString().padStart(2, '0')}` === workLogState.data.date.substr(0, 7))
      .forEach((workLogState: WorkLogState) => {
        employeeId = workLogState.data.employee
        employeeReport = this.report.find(
          (item: ReportItem) => item.employee && item.employee.id === employeeId
        )

        if (!employeeReport) {
          employeeReport = {
            id: employeeId,
            accounts: [],
            employee: employees.find((employee: Employee) => employee.id === employeeId),
          }
          this.report.push(employeeReport)
        }

        account = employeeReport.accounts.find(
          (accountItem: AccountData) => accountItem.name === workLogState.data.account
        )

        if (account) {
          account.hours += workLogState.data.hours
        } else {
          employeeReport.accounts.push({
            name: workLogState.data.account,
            hours: workLogState.data.hours,
          })
        }
        employeeReport.sum = employeeReport.accounts.reduce(
          (sum: number, acc: AccountData) => acc.hours + sum, 0
        )
      })

    this.report.forEach((item: ReportItem) => item.accounts.sort(
      (a: AccountData, b: AccountData) => a.name.localeCompare(b.name)
    ))

    employees.filter(
      (employee: Employee) => !!employee.onlyWorkAccount
    ).forEach((employee: Employee) => {
      if (employee.id && employee.onlyWorkAccount) {
        let hours: number = 0
        const employeeSchedule = scheduleHours[employee.id]
        const employeeAbsence = absence[employee.id]

        Object.keys(employeeSchedule || {}).forEach((date: string) => {
          if (`${year}-${(month + 1).toString().padStart(2, '0')}` === date.substr(0, 7) && this.holidays.isHoliday(new Date(date)).type !== 'public') {
            hours += employeeSchedule[date] - (
              (employeeAbsence && employeeAbsence[date] && employeeAbsence[date].reduce(
                (sum: number = 0, a: AbsenceItem) => sum + (a.type !== 5 ? a.hours : 0), 0
              )) || 0
            )
          }
        })

        if (hours > 0 && employee.id && employee.onlyWorkAccount) {
          this.report.push({
            id: employee.id,
            accounts: [{
              name: employee.onlyWorkAccount,
              hours,
            }],
            employee,
          })
        }
      }
    })
  }

  render() {
    const {
      className, classes, month, year,
    } = this.props
    const months: Array<{ key: number, text: string }> = []
    const now = new Date()
    let date: Date

    for (let i = -5; i < 2; i += 1) {
      date = new Date(now.getFullYear(), now.getMonth() + i, 1)
      months.push({
        key: yearAndMonthToKey(date.getFullYear(), date.getMonth()),
        text: date.toLocaleDateString('cs-CZ', { month: 'long', year: 'numeric' }),
      })
    }
    return (
      <div className={className}>
        <div className={classes.header}>
          <TextField
            select
            value={yearAndMonthToKey(year, month)}
            onChange={this.onMonthSelectFieldChange}
          >
            {months.map((monthItem: { key: number, text: string }) => (
              <MenuItem key={monthItem.key} value={monthItem.key}>{monthItem.text}</MenuItem>
            ))}
          </TextField>
        </div>
        <div>
          <h1>{(new Date(year, month)).toLocaleDateString('cs-CZ', { month: 'long', year: 'numeric' })}</h1>
          {this.report
            .filter((reportItem: ReportItem) => reportItem.employee)
            .map((reportItem: ReportItem) => (
              <div key={reportItem.id}>
                <h2>
                  {reportItem.employee && reportItem.employee.number}
                  &nbsp;
                  {reportItem.employee && reportItem.employee.firstName}
                  &nbsp;
                  {reportItem.employee && reportItem.employee.lastName}
                  &nbsp;
                </h2>
                <div>
                  {reportItem.accounts.map((account: AccountData, index: number) => (
                    <span key={account.name} className={classes.account}>
                      {index !== 0 && ', '}
                      {account.name}
                      -
                      {account.hours}
                    </span>
                  ))}
                  {reportItem.sum && (
                    <span>
                      {', '}
                      <strong>
                        {'celkem: '}
                        {reportItem.sum}
                      </strong>
                    </span>
                  )}
                </div>
              </div>
            ))}
        </div>
      </div>
    )
  }
}

export default withStyles(printStyles, { media: 'print' })(withStyles(styles)(WorkLogMonthlyReport))
