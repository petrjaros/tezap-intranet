// @flow
import WorkLogMonthlyReport from './WorkLogMonthlyReport'

export default WorkLogMonthlyReport
export type { Props } from './WorkLogMonthlyReport'
