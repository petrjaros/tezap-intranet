// @flow
import React, { Component } from 'react'
import classNames from 'classnames'
import {
  TextField, MenuItem, Table, TableBody, TableHead, TableRow, TableCell, withStyles,
} from '@material-ui/core'
import Holidays from 'date-holidays'
import type {
  WorkLogState,
  Employees,
  Employee,
  AbsenceItem,
} from '../../types'

const styles = () => ({
  header: { },
  tableHeadRow: { },
  tableRow: {
    height: 'auto',
    '&:hover': {
      backgroundColor: 'lightGray',
    },
  },
  tableCell: {
    paddingLeft: 5,
    paddingRight: 5,
    '&:last-child': {
      paddingRight: 5,
    },
  },
  account: {
    display: 'inline-block',
    marginRight: '4px',
  },
  date: {
    width: 90,
  },
  accounts: { },
  absence: {
    width: 45,
  },
  accountsSum: {
    width: 30,
  },
  scheduleHoursSum: {
    width: 30,
  },
  warning: {
    '& $accountsSum, & $scheduleHoursSum': {
      color: 'red',
      fontWeight: 'bold',
    },
  },
  weekend: {
    backgroundColor: 'lightyellow',
  },
  holiday: {
    backgroundColor: 'lightgreen',
  },
})

export type Props = {|
  className: string,
  classes: $ObjMap<$Call<typeof styles>, () => string>,
  employees: Employees,
  month: number,
  year: number,
  workLog: Array<WorkLogState>,
  scheduleHours: { [number]: { [string]: number } },
  absence: { [number]: { [string]: Array<AbsenceItem> }},
  onNeedEmployeeData: () => void,
  onNeedWorkLogData: () => void,
  onMonthChange: (year: number, month: number) => void,
  onNeedScheduleData: () => void,
  onNeedAbsenceData: () => void,
|}

type AccountReport = { name: string, hours: number }
type DateReport = {
  date: Date,
  dateString: string,
  isHoliday: boolean,
  isWeekend: boolean,
  accounts: Array<AccountReport>,
  accountsSum: number,
  scheduleHoursSum: number,
  absenceSum: number,
  unpaidVacation: number,
}
type EmployeeReport = { id: number, dateReports: Array<DateReport>, employee: ?Employee }
type Report = {| [number]: EmployeeReport |}

function yearAndMonthToKey(year: number, month: number): number {
  return year * 12 + month
}

function keyToMonth(key: number): number {
  return key % 12
}

function keyToYear(key: number): number {
  return Math.floor(key / 12)
}

class Dashboard extends Component<Props> {
  holidays = new Holidays('CZ')

  isHolidayCache = {}

  toLocaleDateStringCache = {}

  static defaultProps = {
    onNeedEmployeeData: () => {},
    onNeedWorkLogData: () => {},
    onNeedScheduleData: () => {},
    onNeedAbsenceData: () => {},
  }

  constructor(props: Props) {
    super(props)
    this.calculateReport(
      props.workLog || [],
      props.employees || [],
      props.scheduleHours,
      props.absence,
      props.year,
      props.month
    )
  }

  componentDidMount() {
    const {
      onNeedEmployeeData,
      onNeedWorkLogData,
      onNeedScheduleData,
      onNeedAbsenceData,
    } = this.props

    onNeedEmployeeData()
    onNeedWorkLogData()
    onNeedScheduleData()
    onNeedAbsenceData()
  }

  componentWillReceiveProps(nextProps: Props) {
    this.calculateReport(
      nextProps.workLog,
      nextProps.employees,
      nextProps.scheduleHours,
      nextProps.absence,
      nextProps.year,
      nextProps.month
    )
  }

  onMonthSelectFieldChange = (event: SyntheticInputEvent<TextField>) => {
    const { onMonthChange } = this.props
    const { value } = event.target

    if (typeof value === 'number') {
      onMonthChange(keyToYear(value), keyToMonth(value))
    }
  }

  report: Report

  findOrCreateDateReport(employeeId: number, date: string, employees: Employees): DateReport {
    let employeeReport: ?EmployeeReport
    let dateReport: ?DateReport

    if (this.report[employeeId]) {
      employeeReport = this.report[employeeId]
    } else {
      employeeReport = {
        id: employeeId,
        dateReports: [],
        employee: employees.find((employee: Employee) => employee.id === employeeId),
      }
      this.report[employeeId] = employeeReport
    }

    const dayIndex = parseInt(date.substr(8), 10) - 1

    if (employeeReport.dateReports[dayIndex]) {
      dateReport = employeeReport.dateReports[dayIndex]
    } else {
      const dateObject = new Date(date)
      dateReport = {
        date: dateObject,
        dateString: this.toLocaleDateString(date, dateObject),
        isHoliday: this.isHoliday(date),
        isWeekend: dateObject.getDay() === 0 || dateObject.getDay() === 6,
        accounts: [],
        accountsSum: 0,
        scheduleHoursSum: 0,
        absenceSum: 0,
        unpaidVacation: 0,
      }
      employeeReport.dateReports[dayIndex] = dateReport
    }

    return dateReport
  }

  isHoliday(date: string) {
    if (this.isHolidayCache[date] === undefined) {
      this.isHolidayCache[date] = this.holidays.isHoliday(new Date(date)).type === 'public'
    }
    return this.isHolidayCache[date]
  }

  toLocaleDateString(dateString: string, date: Date) {
    if (!this.toLocaleDateStringCache[dateString]) {
      this.toLocaleDateStringCache[dateString] = date.toLocaleDateString('cs-CZ')
    }
    return this.toLocaleDateStringCache[dateString]
  }

  calculateReport(
    workLog: Array<WorkLogState>,
    employees: Employees,
    scheduleHours: { [number]: { [string]: number } },
    absence: { [number]: { [string]: Array<AbsenceItem> }},
    year: number,
    month: number
  ) {
    // $FlowFixMe
    this.report = {}

    workLog
      .filter((workLogState: WorkLogState) => `${year}-${(month + 1).toString().padStart(2, '0')}` === workLogState.data.date.substr(0, 7))
      .forEach((workLogState: WorkLogState) => {
        const employeeId = workLogState.data.employee
        const dateReport = this.findOrCreateDateReport(
          employeeId, workLogState.data.date, employees
        )
        const account = dateReport.accounts.find(
          (accountItem: AccountReport) => accountItem.name === workLogState.data.account
        )

        if (account) {
          account.hours += workLogState.data.hours
        } else {
          dateReport.accounts.push({
            name: workLogState.data.account,
            hours: workLogState.data.hours,
          })
        }
        dateReport.accountsSum += workLogState.data.hours
      })

    Object.keys(this.report).forEach((employeeId: string) => {
      this.report[parseInt(employeeId, 10)].dateReports.forEach(
        (dateReport: DateReport) => dateReport.accounts.sort(
          (a: AccountReport, b: AccountReport) => a.name.localeCompare(b.name)
        )
      )
    })

    employees.filter((employee: Employee) => !!employee.onlyWorkAccount).forEach(
      (employee: Employee) => {
        if (employee.id && employee.onlyWorkAccount) {
          const { id: employeeId, onlyWorkAccount } = employee
          const employeeSchedule = scheduleHours[employee.id]
          const employeeAbsence = absence[employee.id]

          Object.keys(employeeSchedule || {}).forEach((date: string) => {
            if (`${year}-${(month + 1).toString().padStart(2, '0')}` === date.substr(0, 7) && !this.isHoliday(date)) {
              const hours = employeeSchedule[date] - (
                (employeeAbsence && employeeAbsence[date] && employeeAbsence[date].reduce(
                  (sum: number = 0, a: AbsenceItem) => sum + (a.type !== 5 ? a.hours : 0), 0
                )
                ) || 0
              )
              const dateReport = this.findOrCreateDateReport(employeeId, date, employees)

              dateReport.accounts.push({
                name: onlyWorkAccount,
                hours,
              })
              dateReport.accountsSum = hours
            }
          })
        }
      }
    )

    Object.keys(scheduleHours).forEach((employeeId: string) => {
      const employeeHours = scheduleHours[parseInt(employeeId, 10)]
      Object.keys(employeeHours).forEach((date: string) => {
        if (`${year}-${(month + 1).toString().padStart(2, '0')}` === date.substr(0, 7)) {
          const dateReport = this.findOrCreateDateReport(parseInt(employeeId, 0), date, employees)
          if (!this.isHoliday(date)) {
            dateReport.scheduleHoursSum = employeeHours[date]
          }
        }
      })
    })

    Object.keys(absence).forEach((employeeId: string) => {
      const employeeAbsence = absence[parseInt(employeeId, 10)]
      Object.keys(employeeAbsence).forEach((date: string) => {
        if (`${year}-${(month + 1).toString().padStart(2, '0')}` === date.substr(0, 7)) {
          const dateReport = this.findOrCreateDateReport(parseInt(employeeId, 0), date, employees)
          dateReport.absenceSum = employeeAbsence[date].reduce(
            (sum: number = 0, a: AbsenceItem) => sum + (a.type !== 5 ? a.hours : 0),
            0
          )
          dateReport.unpaidVacation = employeeAbsence[date].reduce(
            (sum: number = 0, a: AbsenceItem) => sum + (a.type === 5 ? a.hours : 0),
            0
          )
        }
      })
    })
  }

  render() {
    const {
      className,
      year,
      month,
      classes,
    } = this.props
    const months: Array<{ key: number, text: string }> = []
    const now = new Date()
    let date: Date

    for (let i = -5; i < 2; i += 1) {
      date = new Date(now.getFullYear(), now.getMonth() + i, 1)
      months.push({
        key: yearAndMonthToKey(date.getFullYear(), date.getMonth()),
        text: date.toLocaleDateString('cs-CZ', { month: 'long', year: 'numeric' }),
      })
    }
    return (
      <div className={className}>
        <div className={classes.header}>
          <TextField
            value={yearAndMonthToKey(year, month)}
            select
            onChange={this.onMonthSelectFieldChange}
          >
            {months.map((monthItem: { key: number, text: string }) => (
              <MenuItem key={monthItem.key} value={monthItem.key}>{monthItem.text}</MenuItem>
            ))}
          </TextField>
        </div>
        <Table>
          <TableHead>
            <TableRow className={classes.tableHeadRow}>
              <TableCell classes={{ root: classes.tableCell }} className={classes.date}>
                Datum
              </TableCell>
              <TableCell classes={{ root: classes.tableCell }} className={classes.accounts}>
                Konta
              </TableCell>
              <TableCell classes={{ root: classes.tableCell }} className={classes.absence}>
                Neodpr.
              </TableCell>
              <TableCell classes={{ root: classes.tableCell }} className={classes.accountsSum}>
                Odpr.
              </TableCell>
              <TableCell classes={{ root: classes.tableCell }} className={classes.scheduleHoursSum}>
                Pích.
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Object.keys(this.report)
              .filter((employeeId: string) => this.report[parseInt(employeeId, 10)].employee)
              .map((employeeId: string) => {
                const reportItem = this.report[parseInt(employeeId, 10)]

                return ([
                  <TableRow className={classes.tableHeadRow} key="name">
                    <TableCell component="th" colSpan="5">
                      <strong>
                        {reportItem.employee && reportItem.employee.number}
                        &nbsp;
                        {reportItem.employee && reportItem.employee.firstName}
                        &nbsp;
                        {reportItem.employee && reportItem.employee.lastName}
                        &nbsp;
                      </strong>
                    </TableCell>
                  </TableRow>,
                  ...reportItem.dateReports.map((dateReport: DateReport) => (
                    <TableRow
                      className={classNames(
                        classes.tableRow,
                        {
                          [classes.warning]:
                            dateReport.scheduleHoursSum - dateReport.absenceSum
                            !== dateReport.accountsSum,
                          [classes.weekend]: dateReport.isWeekend,
                          [classes.holiday]: dateReport.isHoliday,
                        }
                      )}
                      key={dateReport.date.toISOString()}
                    >
                      <TableCell classes={{ root: classes.tableCell }} className={classes.date}>
                        {dateReport.dateString}
                      </TableCell>
                      <TableCell classes={{ root: classes.tableCell }} className={classes.accounts}>
                        {dateReport.accounts.map((account: AccountReport, index: number) => (
                          <span key={account.name} className={classes.account}>
                            {account.name}
                            -
                            {account.hours}
                            {index !== dateReport.accounts.length - 1 && ','}
                          </span>
                        ))}
                      </TableCell>
                      <TableCell classes={{ root: classes.tableCell }} className={classes.absence}>
                        {dateReport.absenceSum + dateReport.unpaidVacation}
                      </TableCell>
                      <TableCell
                        classes={{ root: classes.tableCell }}
                        className={classes.accountsSum}
                      >
                        {dateReport.accountsSum}
                      </TableCell>
                      <TableCell
                        classes={{ root: classes.tableCell }}
                        className={classes.scheduleHoursSum}
                      >
                        {Math.max(dateReport.scheduleHoursSum - dateReport.absenceSum, 0)}
                      </TableCell>
                    </TableRow>
                  )),
                ])
              })}
          </TableBody>
        </Table>
      </div>
    )
  }
}

export default withStyles(styles)(Dashboard)
