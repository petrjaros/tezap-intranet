// @flow
import React, { Component } from 'react'
import WorkAccountListItem from './WorkAccountListItem'
import { generateId } from '../../utils'
import type { WorkAccount } from '../../types'
import type { WorkAccountState } from '../../reducers/workAccountReducer'

export type Props = {|
  data: ?Array<WorkAccountState>,
  onNeedWorkAccountData: () => void,
  onChange: (number, WorkAccount) => void,
  onRemove: (number, WorkAccount) => void,
|}

class EmployeeList extends Component<Props> {
  static defaultProps = {
    onNeedWorkAccountData: () => {},
    onCreate: () => {},
    onChange: () => {},
    onRemove: () => {},
  }

  componentDidMount() {
    this.props.onNeedWorkAccountData()
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.data && nextProps.data !== this.props.data) {
      if (nextProps.data.find((item: WorkAccountState) => item.clientId === this.newItemId)) {
        this.newItemId = generateId()
      }
    }
  }

  newItemId: number = generateId()

  render() {
    if (!this.props.data) {
      return null
    }

    return (
      <div>
        {[...this.props.data, null].map(
          (item: ?WorkAccountState) =>
            (item ? (
              <WorkAccountListItem
                key={item.clientId}
                clientId={item.clientId}
                creating={item.creating}
                updating={item.updating}
                removing={item.removing}
                data={item.data}
                onChange={this.props.onChange}
                onRemove={this.props.onRemove}
              />
            ) : (
              <WorkAccountListItem
                key={item ? item.clientId : this.newItemId}
                clientId={this.newItemId}
                onChange={this.props.onChange}
                onRemove={this.props.onRemove}
              />
            ))
        )}
      </div>
    )
  }
}

export default EmployeeList
