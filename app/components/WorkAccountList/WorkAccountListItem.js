// @flow
import React, { Component } from 'react'
import {
  TextField, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions,
} from '@material-ui/core'
import type { WorkAccount } from '../../types'

function isNumberValid(number: string): boolean {
  return number !== ''
}

type Props = {|
  clientId: number,
  data: WorkAccount,
  creating: boolean,
  updating: boolean,
  removing: boolean,
  onChange: (number, WorkAccount) => void,
  onRemove: (number, WorkAccount) => void,
|}

type State = {|
  number: string,
  description: string,
  removeDialogOpen: boolean,
|}

class WorkAccountListItem extends Component<Props, State> {
  static defaultProps = {
    data: {
      id: null,
      number: '',
      description: '',
    },
    creating: false,
    updating: false,
    removing: false,
    onChange: () => {},
    onRemove: () => {},
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      number: props.data.number,
      description: props.data.description,
      removeDialogOpen: false,
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    const { data } = this.props

    if (nextProps.data !== data) {
      this.setState({
        number: nextProps.data.number,
        description: nextProps.data.description,
      })
    }
  }

  handleRemoveClick = () => {
    this.setState({
      removeDialogOpen: true,
    })
  }

  handleDialogClose = () => {
    this.setState({
      removeDialogOpen: false,
    })
  }

  handleRemove = () => {
    const { clientId, data, onRemove } = this.props

    onRemove(clientId, data)
  }

  handleNumberChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({
      number: event.target.value,
    })

    if (this.isValid(event.target.value)) {
      this.fireOnChange(event.target.value)
    }
  }

  handleDescriptionChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    this.setState({
      description: event.target.value,
    })

    if (this.isValid()) {
      this.fireOnChange(undefined, event.target.value)
    }
  }

  isValid(number: ?string) {
    const { number: stateNumber } = this.state

    return isNumberValid(number || stateNumber)
  }

  fireOnChange(number: string | typeof undefined, description: string | typeof undefined) {
    const { onChange, clientId, data } = this.props
    const { number: stateNumber, description: stateDescription } = this.state

    onChange(clientId, {
      id: data.id,
      number: number === undefined ? stateNumber : number,
      description: description === undefined ? stateDescription : description,
    })
  }

  render() {
    const { data } = this.props
    const { number, description, removeDialogOpen } = this.state

    return (
      <div>
        <TextField
          value={number || ''}
          onChange={this.handleNumberChange}
          placeholder="Číslo"
          margin="none"
        />
        <TextField
          value={description || ''}
          onChange={this.handleDescriptionChange}
          placeholder="Popis"
          margin="none"
        />
        {data.id && <Button variant="text" onClick={this.handleRemoveClick}>Smazat</Button>}
        <Dialog open={removeDialogOpen} onClose={this.handleDialogClose}>
          <DialogTitle>Opravdu smazat?</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {`Opravdu zamazat konto ${number}?`}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleDialogClose} color="secondary">
              Zavřít
            </Button>
            <Button onClick={this.handleRemove} color="primary" autoFocus>
              Smazat
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default WorkAccountListItem
