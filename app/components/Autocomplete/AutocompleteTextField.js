import React, { Component } from 'react'
import classNames from 'classnames'
import Input from '@material-ui/core/Input'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import InputLabel from '@material-ui/core/InputLabel'

import Autocomplete from './Autocomplete'

export default class AutocompleteTextField extends Component {
  handleInputLabelCheckboxChange = (event, checked) => {
    const { defaultValue, onChange } = this.props
    const value = checked ? defaultValue || 0 : null
    event.persist()
    event.target = { value } // eslint-disable-line no-param-reassign
    onChange(event, value)
  }

  render() {
    const {
      autoComplete,
      AutocompleteProps,
      autoFocus,
      children,
      classes,
      className,
      defaultValue,
      error,
      FormHelperTextProps,
      fullWidth,
      helperText,
      id,
      InputLabelProps,
      inputProps,
      InputProps,
      inputRef,
      label,
      labelTooltip,
      name,
      nullable,
      onBlur,
      onChange,
      onFocus,
      placeholder,
      required,
      shrinkLabel,
      type,
      value,
      ...other
    } = this.props

    const helperTextId = helperText && id ? `${id}-helper-text` : undefined
    const InputElement = (
      <Input
        autoComplete={autoComplete}
        autoFocus={autoFocus}
        classes={{
          ...(InputProps || {}).classes,
          input: classNames(
            InputProps && InputProps.classes && InputProps.classes.input,
          ),
        }}
        defaultValue={defaultValue}
        fullWidth={fullWidth}
        name={name}
        type={type}
        value={value || ''}
        id={id}
        inputRef={inputRef}
        onBlur={onBlur}
        onChange={onChange}
        onFocus={onFocus}
        placeholder={placeholder}
        inputProps={inputProps}
        disabled={nullable && value === null ? true : undefined}
        {...InputProps}
      />
    )
    const WrappedElement = (
      <Autocomplete
        value={value}
        input={InputElement}
        onChange={onChange}
        allowCustomValues
        {...AutocompleteProps}
      >
        {children}
      </Autocomplete>
    )

    return (
      <FormControl
        aria-describedby={helperTextId}
        className={className}
        error={error}
        fullWidth={fullWidth}
        required={required}
        variant="standard"
        {...other}
      >
        {label && (
          <InputLabel
            htmlFor={id}
            tooltip={labelTooltip}
            showCheckbox={nullable}
            checkboxChecked={value !== null}
            shrink={placeholder || nullable ? true : shrinkLabel}
            onCheckboxChange={this.handleInputLabelCheckboxChange}
            {...InputLabelProps}
          >
            {label}
          </InputLabel>
        )}
        {WrappedElement}
        <FormHelperText
          id={helperTextId}
          {...FormHelperTextProps}
        >
          {helperText}
        </FormHelperText>
      </FormControl>
    )
  }
}

AutocompleteTextField.defaultProps = {
  required: false,
}
