import React, { PureComponent, Fragment } from 'react'
import classNames from 'classnames'
import keycode from 'keycode'
import Autosuggest from 'react-autosuggest'
import { withStyles } from '@material-ui/core/styles'
import Input from '@material-ui/core/Input'
import Paper from '@material-ui/core/Paper'
import Fade from '@material-ui/core/Fade'
import Popper from '@material-ui/core/Popper'
import { formControlState } from '@material-ui/core/InputBase/InputBase'

import ToggleIcon from './ToggleIcon'

function itemsValuesAreEqual(itemValue1, itemValue2) {
  return itemValue1.toLowerCase() === itemValue2.toLowerCase()
}

function getSuggestionValue(suggestion) {
  return suggestion.props.value
}

function findInSuggestionItems(suggestionItems, searchValue) {
  return suggestionItems.find(
    (item) => itemsValuesAreEqual(getSuggestionValue(item), searchValue.trimStart())
  )
}

function filterSuggestions(suggestions, inputValue) {
  return suggestions.filter(
    (suggestion) => (
      !inputValue || getSuggestionValue(suggestion).toLowerCase().includes(inputValue.toLowerCase())
    )
  )
}

const styles = (theme) => ({
  container: {
    display: 'inline-flex',
    position: 'relative',
  },
  containerOpen: {},
  input: {},
  inputOpen: {},
  inputFocused: {},
  suggestionsContainer: {},
  suggestionsContainerOpen: {},
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
  suggestion: {},
  suggestionFirst: {},
  suggestionHighlighted: {
    backgroundColor: theme.palette.action.hover,
  },
  sectionContainer: {},
  sectionContainerFirst: {},
  sectionTitle: {},
})

class Autocomplete extends PureComponent {
  static defaultProps = {
    onBlur: () => {},
    onChange: () => {},
    onChangeInputValue: () => {},
    onFocus: () => {},
    onSelect: () => {},
  }

  static getDerivedStateFromProps(props) {
    if (props.disabled) {
      return {
        inputValue: '',
      }
    }
    return null
  }

  constructor(props) {
    super(props)

    const { value, inputRef } = this.props

    this.state = {
      inputValue: value,
      isOpen: false,
    }
    this.hasFocus = false
    this.inputRef = inputRef || React.createRef()
    this.popper = null
    this.highlightedSuggestionItem = null
  }

  componentDidMount() {
    const { autoFocus } = this.props

    if (autoFocus && this.inputRef.current) {
      this.inputRef.current.focus()
    }
  }

  getInputValue() {
    const { inputValue } = this.props
    const { inputValue: stateInputValue } = this.state

    return inputValue !== undefined ? inputValue : stateInputValue
  }

  setInputValue(inputValue) {
    const { inputValue: propsInputValue, onChangeInputValue } = this.props

    if (propsInputValue !== undefined) {
      onChangeInputValue(inputValue)
    } else {
      this.setState({ inputValue })
    }
  }

  handleInputFocus = (event) => {
    const { input, value, onFocus } = this.props

    this.hasFocus = true
    this.setInputValue(value)
    this.setState({ isOpen: true })
    onFocus(event)

    if (input && input.props.onFocus) {
      input.props.onFocus(event)
    }
  }

  handleInputBlur = (event) => {
    const { input, onBlur, value } = this.props

    this.hasFocus = false
    this.setInputValue(value)
    this.setState({
      isOpen: false,
    })
    onBlur(event)

    if (input && input.props.onBlur) {
      input.props.onBlur(event)
    }
  }

  handleInputChange = (event, { newValue, method }) => {
    const { allowCustomValues, name, onChange } = this.props

    if (['type', 'enter', 'click'].indexOf(method) !== -1) {
      this.setInputValue(newValue)
      this.setState({
        isOpen: true,
      })

      if (method === 'type' && (
        allowCustomValues
        || findInSuggestionItems(this.childrenArray, newValue)
      )) {
        event.persist()
        event.target = { value: newValue, name } // eslint-disable-line no-param-reassign
        onChange(event, this.childrenArray.find((child) => getSuggestionValue(child) === newValue))
      }

      if (['enter', 'click'].indexOf(method) !== -1) {
        this.selectValue(event, newValue)

        this.setState({
          isOpen: false,
        })
      }
    }

    if (['down', 'up'].indexOf(method) !== -1) {
      this.setState({
        isOpen: true,
      })
    }
  }

  handleInputMouseDown = () => {
    const { isOpen } = this.state

    if (this.hasFocus) {
      this.setState({
        isOpen: !isOpen,
      })
    }
  }

  handleInputKeyDown = (event) => {
    const { value } = this.props

    if (keycode(event) === 'esc') {
      this.setInputValue(value)
      this.setState({
        isOpen: false,
      })
    }
  }

  handleSuggestionHighlighted = ({ suggestion }) => {
    this.highlightedSuggestionItem = suggestion
  }

  handleSuggestionSelected = (event, { suggestionValue }) => {
    const { allowCustomValues } = this.props

    if (suggestionValue === this.getInputValue() && !allowCustomValues) {
      this.selectValue(event, this.getInputValue())
    }
  }

  handlePopperCreate = (data) => {
    this.popper = data.popper
  }

  selectValue(event, newValue) {
    const { onChange, name } = this.props
    const value = newValue

    event.persist()
    event.target = { value, name } // eslint-disable-line no-param-reassign
    onChange(event, this.childrenArray.find((child) => getSuggestionValue(child) === newValue))
  }

  isValueSelected(value) {
    const { value: propsValue } = this.props
    return propsValue === value
  }

  renderInputComponent = (inputProps) => {
    const { ref, ...restProps } = inputProps
    const { hideToggleIcon } = this.props
    const { isOpen } = this.state

    const inputRef = (node) => {
      inputProps.ref(node)
      this.inputRef.current = node
    }

    return (
      <Fragment>
        <Input {...restProps} inputRef={inputRef} />
        {!hideToggleIcon && <ToggleIcon isOpen={isOpen} />}
      </Fragment>
    )
  }

  renderSuggestionsContainer = ({ containerProps, children }) => {
    const { autoWidth } = this.props
    const { isOpen } = this.state

    return (
      <Popper
        style={{
          zIndex: 1,
          minWidth: this.inputRef.current && !autoWidth ? this.inputRef.current.clientWidth : null,
        }}
        placement="bottom-start"
        open={isOpen}
        // disablePortal
        anchorEl={this.inputRef.current}
        popperOptions={{ onCreate: this.handlePopperCreate }}
        transition
      >
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={350}>
            <Paper {...containerProps} square>
              {children}
            </Paper>
          </Fade>
        )}
      </Popper>
    )
  }

  renderSuggestion = (suggestion) => {
    const {
      value,
    } = this.props

    return React.cloneElement(suggestion, {
      selected: value === suggestion.props.value,
      component: 'div',
    })
  }

  render() {
    const {
      children,
      classes,
      className,
      name,
      placeholder,
      type,
    } = this.props

    const fcs = formControlState({
      props: this.props,
      context: this.context,
      states: ['disabled'],
    })

    this.childrenArray = React.Children.map(children, (child) => {
      if (!React.isValidElement(child)) {
        return null
      }

      return child
    })
    const suggestions = filterSuggestions(this.childrenArray, this.getInputValue())

    return (
      <Autosuggest
        alwaysRenderSuggestions
        suggestions={suggestions}
        inputProps={{
          disabled: fcs.disabled,
          name,
          placeholder,
          type,
          value: this.getInputValue(),
          onBlur: this.handleInputBlur,
          onChange: this.handleInputChange,
          onMouseDown: this.handleInputMouseDown,
          onFocus: this.handleInputFocus,
          onKeyDown: this.handleInputKeyDown,
        }}
        inputRef={this.inputRef}
        theme={{
          ...classes,
          container: classNames(classes.container, className),
        }}
        getSuggestionValue={getSuggestionValue}
        renderInputComponent={this.renderInputComponent}
        renderSuggestion={this.renderSuggestion}
        renderSuggestionsContainer={this.renderSuggestionsContainer}
        onSuggestionsFetchRequested={() => {}}
        onSuggestionHighlighted={this.handleSuggestionHighlighted}
        onSuggestionSelected={this.handleSuggestionSelected}
      />
    )
  }
}

export default withStyles(styles)(Autocomplete)
