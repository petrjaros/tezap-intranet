import React from 'react'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import ArrowDropDownIcon from '@material-ui/core/internal/svg-icons/ArrowDropDown'

const styles = (theme) => ({
  icon: {
    position: 'absolute',
    right: 0,
    color: theme.palette.action.active,
    'pointer-events': 'none', // don't block pointer events on the select under the icon.
    width: 18,
    height: 18,
    top: 0,
    bottom: 0,
    margin: [['auto', 8, 'auto', 0]],
    transition: theme.transitions.create(['transform'], {
      duration: theme.transitions.duration.longer,
    }),
  },
  iconOpen: {
    transform: 'rotateX(180deg)',
  },
})

function ToggleIcon(props) {
  const { classes, isOpen } = props

  return (
    <ArrowDropDownIcon
      className={classNames(
        classes.icon,
        {
          [classes.iconOpen]: isOpen,
        }
      )}
    />
  )
}

export default withStyles(styles)(ToggleIcon)
