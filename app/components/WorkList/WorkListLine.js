// @flow
import React, { Component } from 'react'
import {
  withStyles,
  TextField,
  Button,
  MenuItem,
} from '@material-ui/core'
import { AutocompleteTextField } from '../Autocomplete'
import type { WorkAccounts, WorkAccount } from '../../types'

const styles = {
  account: {
    width: '150px',
  },
  suggestionsList: {
    height: 'auto',
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    maxHeight: '90vh',
    width: '80vw',
  },
  suggestionItem: { },
}

function isHoursValid(hours: ?number): boolean {
  if (!hours) {
    return false
  }
  return hours > 0 && Math.round(hours * 2) === hours * 2
}

function isAccountValid(workAccounts: WorkAccounts, account: string): boolean {
  return (
    account !== '' && !!workAccounts.find((workAccount: WorkAccount) => workAccount.number === account)
  )
}

export type WorkLine = {|
  clientId: number,
  account: ?string,
  hours: ?number,
|}

type Props = {|
  classes: $ObjMap<typeof styles, () => string>,
  workAccounts: WorkAccounts,
  data: WorkLine,
  onChange: WorkLine => void,
  onRemove: WorkLine => void,
|}

type State = {|
  account: string,
  hours: ?number,
|}

class WorkListLine extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      account: props.data.account || '',
      hours: props.data.hours,
    }
  }

  componentWillReceiveProps(nextProps: Props) {
    const { data } = this.props

    if (nextProps.data !== data) {
      this.setState({
        account: nextProps.data.account || '',
        hours: nextProps.data.hours,
      })
    }
  }

  handleRemoveClick = () => {
    const { data, onRemove } = this.props
    const { account, hours } = this.state

    onRemove({
      clientId: data.clientId,
      account,
      hours,
    })
  }

  handleAccountChange = (event: SyntheticInputEvent<TextField>) => {
    const { value } = event.target

    this.setState({
      account: value,
    })

    if (this.isValid(value)) {
      this.fireOnChange(value)
    }
  }

  handleHoursChange = (event: SyntheticInputEvent<TextField>) => {
    const hours: number = parseFloat(event.target.value) || 0
    this.setState({
      hours,
    })

    if (this.isValid(undefined, hours)) {
      this.fireOnChange(undefined, hours)
    }
  }

  isValid(account: ?string, hours: ?number) {
    const { workAccounts } = this.props
    const { account: stateAccount, hours: stateHours } = this.state

    return (
      isAccountValid(workAccounts, account || stateAccount) && isHoursValid(hours || stateHours)
    )
  }

  textFieldInput: typeof Component

  fireOnChange(account: string | typeof undefined, hours: number | typeof undefined) {
    const { onChange, data } = this.props
    const { account: stateAccount, hours: stateHours } = this.state

    onChange({
      clientId: data.clientId,
      account: account === undefined ? stateAccount : account,
      hours: hours === undefined ? stateHours : hours,
    })
  }

  render() {
    const { classes, workAccounts } = this.props
    const { account, hours } = this.state

    return (
      <div>
        <AutocompleteTextField
          value={account}
          onChange={this.handleAccountChange}
          helperText="Konto"
          error={!!account && !isAccountValid(workAccounts, account)}
          classes={{
            container: classes.account,
          }}
          AutocompleteProps={{
            classes: {
              suggestionsList: classes.suggestionsList,
            },
          }}
        >
          {workAccounts.map((workAccount: WorkAccount) => (
            <MenuItem
              key={workAccount.id}
              value={workAccount.number}
              className={classes.suggestionItem}
            >
              {`${workAccount.number} ${workAccount.description}`}
            </MenuItem>
          ))}
        </AutocompleteTextField>
        <TextField
          type="number"
          value={hours || ''}
          onChange={this.handleHoursChange}
          helperText="Hodiny"
          error={!!hours && !isHoursValid(hours)}
          ref={(input: typeof TextField) => {
            this.textFieldInput = input
          }}
          style={{
            width: '100px',
          }}
        />
        <Button variant="text" onClick={this.handleRemoveClick} tabIndex={-1}>Smazat</Button>
      </div>
    )
  }
}

export default withStyles(styles)(WorkListLine)
