// @flow
import React, { Component } from 'react'
import { withStyles } from '@material-ui/core'
import WorkListLine from './WorkListLine'
import { generateId } from '../../utils'
import type { WorkAccounts } from '../../types'
import type { WorkLine } from './WorkListLine'

export type Work = Array<WorkLine>

type Props = {|
  workAccounts: WorkAccounts,
  data: Work,
  onChange: WorkLine => void,
  onRemove: WorkLine => void,
|}

type Classes = {|
  classes: {|
    workList: string,
  |},
|}

const styles = () => ({
  workList: {
    marginLeft: '15px',
  },
})

class WorkList extends Component<Props & Classes> {
  newItemId: number = generateId()

  componentWillReceiveProps(nextProps: Props) {
    const { data } = this.props

    if (nextProps.data && nextProps.data !== data) {
      if (nextProps.data.find((item: WorkLine) => item.clientId === this.newItemId)) {
        this.newItemId = generateId()
      }
    }
  }

  render() {
    const {
      data: propsData, classes, workAccounts, onChange, onRemove,
    } = this.props
    const data = [...propsData, { clientId: this.newItemId, account: '', hours: null }]
    return (
      <div className={classes.workList}>
        {data.map((item: WorkLine) => (
          <WorkListLine
            key={item.clientId}
            workAccounts={workAccounts}
            data={item}
            onChange={onChange}
            onRemove={onRemove}
          />
        ))}
      </div>
    )
  }
}

export default withStyles(styles)(WorkList)
