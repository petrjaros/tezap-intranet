// @flow
import React, { Component } from 'react'
import {
  withStyles, TextField, MenuItem, Button,
} from '@material-ui/core'
import Holidays from 'date-holidays'
import WorkList from '../WorkList'
import { toISODateString } from '../../utils/formatters'
import type { WorkLine } from '../WorkList'
import type {
  WorkLogItem,
  WorkLogState,
  AbsenceItem,
  Employees,
  Employee,
  WorkAccounts,
} from '../../types'

export type Props = {|
  className: string,
  employee: number,
  date: Date,
  workLog: ?Array<WorkLogState>,
  scheduleHours: ?number,
  absence: ?Array<AbsenceItem>,
  employees: Employees,
  workAccounts: WorkAccounts,
  onNeedEmployeeData: () => void,
  onNeedWorkAccountData: () => void,
  onNeedWorkLogData: () => void,
  onNeedScheduleData: () => void,
  onNeedAbsenceData: () => void,
  onWorkChange: (clientId: number, workLog: WorkLogItem) => void,
  onWorkRemove: (clientId: number, workLog: WorkLogItem) => void,
  onChangeEmployee: (employee: number) => void,
  onChangeDate: (date: Date) => void,
|}

type Classes = {|
  classes: {|
    body: string,
    header: string,
    date: string,
    day: string,
    content: string,
    summary: string,
  |},
|}

const styles = () => ({
  body: {
    maxWidth: '1200px',
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  date: {
    display: 'flex',
    alignItems: 'baseline',
  },
  day: {
    display: 'inline-block',
    width: 35,
  },
  content: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  summary: {
    marginTop: '50px',
    width: '200px',
    marginLeft: 'auto',
    right: 0,
  },
})

class WorkLogEdit extends Component<Props & Classes> {
  static defaultProps = {
    onNeedEmployeeData: () => {},
    onNeedWorkAccountData: () => {},
    onNeedWorkLogData: () => {},
    onNeedScheduleData: () => {},
    onNeedAbsenceData: () => {},
  }

  holidays = new Holidays('CZ')

  componentDidMount() {
    const {
      onNeedEmployeeData,
      onNeedWorkAccountData,
      onNeedWorkLogData,
      onNeedScheduleData,
      onNeedAbsenceData,
    } = this.props

    onNeedEmployeeData()
    onNeedWorkAccountData()
    onNeedWorkLogData()
    onNeedScheduleData()
    onNeedAbsenceData()
  }

  handleEmployeeChange = (event: SyntheticInputEvent<TextField>) => {
    const { onChangeEmployee } = this.props
    onChangeEmployee(parseInt(event.target.value, 10))
  }

  handleDateChange = (event: SyntheticInputEvent<TextField>) => {
    const { onChangeDate } = this.props
    onChangeDate(new Date(event.target.value))
  }

  handlePreviousClick = () => {
    const { date, onChangeDate } = this.props
    const increment = date.getDay() === 1 ? -3 : -1
    onChangeDate(
      new Date(date.getFullYear(), date.getMonth(), date.getDate() + increment)
    )
  }

  handleNextClick = () => {
    const { date, onChangeDate } = this.props
    const increment = date.getDay() === 5 ? 3 : 1
    onChangeDate(
      new Date(date.getFullYear(), date.getMonth(), date.getDate() + increment)
    )
  }

  handleWorkChange = (work: WorkLine) => {
    const {
      date, employee, workLog, onWorkChange,
    } = this.props
    const workLogItem: ?WorkLogState = workLog && workLog.find(
      (item: WorkLogState) => item.clientId === work.clientId
    )
    onWorkChange(work.clientId, {
      id: workLogItem ? workLogItem.data.id : null,
      date: workLogItem ? workLogItem.data.date : toISODateString(date),
      employee: workLogItem ? workLogItem.data.employee : employee,
      account: work.account || '',
      hours: work.hours || 0,
    })
  }

  handleWorkRemove = (work: WorkLine) => {
    const { workLog, onWorkRemove } = this.props
    const workLogItem: ?WorkLogState = workLog && workLog.find(
      (item: WorkLogState) => item.clientId === work.clientId
    )
    if (workLogItem) {
      onWorkRemove(work.clientId, {
        id: workLogItem.data.id,
        date: workLogItem.data.date,
        employee: workLogItem.data.employee,
        account: work.account || '',
        hours: work.hours || 0,
      })
    }
  }

  render() {
    const {
      className,
      classes,
      workLog,
      scheduleHours,
      absence,
      date,
      employee,
      employees,
      workAccounts,
    } = this.props
    let absenceSum = 0
    let workLogSum = 0
    let unpaidVacation = 0

    if (absence) {
      absence.forEach((absenceItem: AbsenceItem) => {
        if (absenceItem.type === 5) {
          unpaidVacation += absenceItem.hours
        } else {
          absenceSum += absenceItem.hours
        }
      })
    }
    absenceSum = Math.round(absenceSum * 2) / 2.0
    unpaidVacation = Math.round(unpaidVacation * 2) / 2.0

    if (this.holidays.isHoliday(date).type === 'public') {
      absenceSum = scheduleHours || 0
    }

    if (workLog) {
      workLog.forEach((workLogItem: WorkLogState) => {
        workLogSum += workLogItem.data.hours || 0
      })
    }

    return (
      <div className={`${className} ${classes.body}`}>
        <div className={classes.header}>
          <Button variant="text" onClick={this.handlePreviousClick}>Předchozí</Button>
          <TextField
            select
            label="Jméno"
            value={employee}
            onChange={this.handleEmployeeChange}
          >
            {employees
              .filter((employeesItem: Employee) => employeesItem.working)
              .map((employeesItem: Employee) => (
                <MenuItem key={employeesItem.id} value={employeesItem.id}>
                  {`${employeesItem.firstName} ${employeesItem.lastName}`}
                </MenuItem>
              ))
            }
          </TextField>
          <div className={classes.date}>
            <span className={classes.day}>
              (
              {['ne', 'po', 'út', 'st', 'čt', 'pá', 'so'][date.getDay()]}
              )
            </span>
            <TextField
              type="date"
              label="Datum"
              value={toISODateString(date)}
              onChange={this.handleDateChange}
              required
            />
          </div>
          <Button type="text" onClick={this.handleNextClick}>Další</Button>
        </div>
        <div className={classes.content}>
          {workLog && (
            <WorkList
              workAccounts={workAccounts}
              data={workLog.map((workItem: WorkLogState) => ({
                clientId: workItem.clientId,
                account: workItem.data.account,
                hours: workItem.data.hours,
              }))}
              onChange={this.handleWorkChange}
              onRemove={this.handleWorkRemove}
            />
          )}
          <div className={classes.summary}>
            <div>
              <strong>{`Zadáno: ${workLogSum}`}</strong>
            </div>
            <div>
              <strong>{`Z píchaček: ${Math.max((scheduleHours || 0) - absenceSum, 0)}`}</strong>
            </div>
            <div>{`Absence: ${absenceSum + unpaidVacation}`}</div>
            <div>{`Celkem: ${scheduleHours + unpaidVacation}`}</div>
          </div>
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(WorkLogEdit)
