// @flow
import React from 'react'
import { Link } from 'react-router-dom'
import {
  withStyles,
  List,
  ListItem,
  ListItemText,
} from '@material-ui/core'

const styles = () => ({
  list: {
    width: '100%',
    maxWidth: 192,
  },
})

const printStyles = () => ({
  list: {
    display: 'none',
  },
})

type Props = {|
  classes: {|
    list: string,
  |},
|}

function MainMenu(props: Props) {
  const { classes } = props

  return (
    <List component="nav" className={classes.list}>
      <ListItem button component={Link} to="/">
        <ListItemText primary="Přehled" />
      </ListItem>
      <ListItem button component={Link} to="/zadavani-prace">
        <ListItemText primary="Zadávání práce" />
      </ListItem>
      <ListItem button component={Link} to="/tisk-vykazu">
        <ListItemText primary="Tisk výkazu" />
      </ListItem>
      <ListItem button component={Link} to="/zamestnanci">
        <ListItemText primary="Zaměstnanci" />
      </ListItem>
      <ListItem button component={Link} to="/konta">
        <ListItemText primary="Konta" />
      </ListItem>
    </List>
  )
}

export default withStyles(printStyles, { media: 'print' })(withStyles(styles)(MainMenu))
