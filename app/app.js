// @flow

// Import all the third party stuff
import * as React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'

/* eslint-disable import/no-webpack-loader-syntax */
// $FlowFixMe
import '!file-loader?name=[name].[ext]!./favicon.ico'
/* eslint-enable import/no-webpack-loader-syntax */

import configureStore from './store'
import App from './containers/App'

// Import CSS reset and Global Styles
// import './global-styles';

// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`
const history = createHistory()
const initialState = {}
const store = configureStore(initialState, history)
const appElement = document.getElementById('app')

/* eslint-disable no-underscore-dangle */
window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true
/* eslint-enable */

if (appElement) {
  const render = (Component: typeof App) => {
    ReactDOM.render(
      <Provider store={store}>
        <Component history={history} />
      </Provider>,
      appElement
    )
  }

  render(App)
}
