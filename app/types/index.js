// @flow
import type { Action } from '../actions/actionTypes'

type Dispatch = Action => Promise<Action>

type WorkLogItem = {|
  id: ?number,
  date: string,
  employee: number,
  account: string,
  hours: number,
|}
type WorkLog = Array<WorkLogItem>

type ScheduleItem = {|
  id: ?number,
  date: string,
  employee: number,
  hours: number,
|}
type Schedule = Array<ScheduleItem>

type AbsenceItem = {|
  id: ?number,
  date: string,
  employee: number,
  type: number,
  hours: number,
|}
type Absence = Array<AbsenceItem>

type Employee = {|
  id: ?number,
  number: number,
  firstName: string,
  lastName: string,
  working: number,
  onlyWorkAccount: ?string,
|}
type Employees = Array<Employee>

type WorkAccount = {|
  id: ?number,
  number: string,
  description: string,
|}
type WorkAccounts = Array<WorkAccount>

export type {
  Action,
  Dispatch,
  WorkLogItem,
  WorkLog,
  ScheduleItem,
  Schedule,
  AbsenceItem,
  Absence,
  Employees,
  Employee,
  WorkAccounts,
  WorkAccount,
}
export type { State } from '../reducers'
export type { WorkLogState } from '../reducers/workLogReducer'
