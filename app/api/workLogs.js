// @flow
import type { WorkLog, WorkLogItem } from '../types'
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'

const workLogs: {
  get: HttpResourceType<{}, void, WorkLog>,
  post: HttpResourceType<{}, WorkLogItem, { id: number }>,
} = {
  get: httpResource(`http://${location.host}/worklog`, 'GET'),
  post: httpResource(`http://${location.host}/worklog`, 'POST'),
}

export default workLogs
