// @flow
import type { Schedule } from '../types'
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'

const schedule: {
  get: HttpResourceType<{}, void, Schedule>,
} = {
  get: httpResource(`http://${location.host}/schedule`, 'GET'),
}

export default schedule
