// @flow
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'
import type { WorkLogItem } from '../types'

const workLog: {
  put: HttpResourceType<{ id: number }, WorkLogItem, void>,
  delete: HttpResourceType<{ id: number }, void, void>,
} = {
  put: httpResource(`http://${location.host}/worklog/{id}`, 'PUT'),
  delete: httpResource(`http://${location.host}/worklog/{id}`, 'DELETE'),
}

export default workLog
