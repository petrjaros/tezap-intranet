// @flow
import type { WorkAccount } from '../types'
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'

const workAccount: {
  put: HttpResourceType<{}, WorkAccount, { id: number }>,
  delete: HttpResourceType<{}, WorkAccount, { id: number }>,
} = {
  put: httpResource(`http://${location.host}/work-account/{id}`, 'PUT'),
  delete: httpResource(`http://${location.host}/work-account/{id}`, 'DELETE'),
}

export default workAccount
