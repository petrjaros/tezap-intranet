// @flow
import type { WorkAccounts, WorkAccount } from '../types'
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'

const workAccounts: {
  get: HttpResourceType<{}, void, WorkAccounts>,
  post: HttpResourceType<{}, WorkAccount, { id: number }>,
} = {
  get: httpResource(`http://${location.host}/work-account`, 'GET'),
  post: httpResource(`http://${location.host}/work-account`, 'POST'),
}

export default workAccounts
