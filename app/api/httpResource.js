// @flow
export type HttpResourceType<Params: { [string]: any }, Body, Resp> = (
  routeParams: Params,
  body: Body
) => Promise<?Resp>

function httpResource<Params: { [string]: any }, Body, Resp>(
  urlTemplate: string,
  method: string
): HttpResourceType<Params, Body, Resp> {
  return async (routeParams: Params, body: Body): Promise<?Resp> => {
    const params: { [string]: any } = {
      method,
      Accept: 'application/json',
    }

    if (body) {
      params.headers = {
        'Content-Type': 'application/json',
      }
      params.body = JSON.stringify(body)
    }

    let resolvedUrl: string = urlTemplate
    const matches: ?Array<string> = urlTemplate.match(/\{([^}]+)\}/g)
    if (matches) {
      matches.forEach((val: string) => {
        const paramKey: string = val.substring(1, val.length - 1)
        resolvedUrl = resolvedUrl.replace(
          val,
          routeParams[paramKey] == null ? '' : routeParams[paramKey]
        )
      })
    }
    resolvedUrl = resolvedUrl.replace(/\/*$/, '') // removed trailing slashes if any

    const request = new Request(resolvedUrl, params)

    const response = await fetch(request)

    if (response.ok) {
      return response.json()
    }
    throw response.text()
  }
}

export default httpResource
