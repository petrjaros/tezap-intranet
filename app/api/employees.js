// @flow
import type { Employees, Employee } from '../types'
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'

const employees: {
  get: HttpResourceType<{}, void, Employees>,
  post: HttpResourceType<{}, Employee, { id: number }>,
} = {
  get: httpResource(`http://${location.host}/employee`, 'GET'),
  post: httpResource(`http://${location.host}/employee`, 'POST'),
}

export default employees
