// @flow
import type { Employee } from '../types'
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'

const employee: {
  put: HttpResourceType<{}, Employee, { id: number }>,
  delete: HttpResourceType<{}, Employee, { id: number }>,
} = {
  put: httpResource(`http://${location.host}/employee/{id}`, 'PUT'),
  delete: httpResource(`http://${location.host}/employee/{id}`, 'DELETE'),
}

export default employee
