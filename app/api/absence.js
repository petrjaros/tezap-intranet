// @flow
import type { Absence } from '../types'
import httpResource from './httpResource'
import type { HttpResourceType } from './httpResource'

const absence: {
  get: HttpResourceType<{}, void, Absence>,
} = {
  get: httpResource(`http://${location.host}/absence`, 'GET'),
}

export default absence
