// @flow
import { connect } from 'react-redux'
import WorkLogEdit from '../../components/WorkLogEdit'
import {
  changeEmployee,
  changeDate,
  loadWorkLogs,
  createWorkLog,
  updateWorkLog,
  removeWorkLog,
} from '../../actions/workLogActions'
import { loadEmployees } from '../../actions/employeeActions'
import { loadWorkAccounts } from '../../actions/workAccountActions'
import { loadSchedule } from '../../actions/scheduleActions'
import { loadAbsence } from '../../actions/absenceActions'
import { toISODateString } from '../../utils/formatters'
import { getCurrentWorkLogSelector } from '../../selectors/workLogSelectors'
import { getAvailableEmployeesSelector } from '../../selectors/employeeSelectors'
import { getWorkAccountsSelector } from '../../selectors/workAccountSelectors'
import type {
  Dispatch,
  State,
  WorkLogState,
  WorkLogItem,
  AbsenceItem,
  Employees,
  WorkAccounts,
} from '../../types'
import type { Props as WorkLogEditProps } from '../../components/WorkLogEdit'

type Props = {|
  className: string,
|}

type StateProps = {|
  employee: number,
  date: Date,
  className: string,
  workLogs: ?Array<WorkLogState>,
  workLog: ?Array<WorkLogState>,
  scheduleHours: ?number,
  absence: ?Array<AbsenceItem>,
  employees: Employees,
  workAccounts: WorkAccounts,
|}

function mapStateToProps(state: State, ownProps: Props): StateProps {
  const date = toISODateString(state.workLog.date)
  const employee = state.workLog.employee
  return {
    className: ownProps.className,
    employee,
    date: state.workLog.date,
    workLogs: state.workLog.data,
    workLog: getCurrentWorkLogSelector(state),
    scheduleHours:
      (state.schedule.data &&
        state.schedule.data[employee] &&
        state.schedule.data[employee][date]) ||
      0,
    absence:
      (state.absence.data && state.absence.data[employee] && state.absence.data[employee][date]) ||
      [],
    employees: getAvailableEmployeesSelector(state),
    workAccounts: getWorkAccountsSelector(state),
  }
}

export type DispatchProps = {|
  onNeedEmployeeData: () => void,
  onNeedWorkAccountData: () => void,
  onNeedWorkLogData: () => void,
  onNeedScheduleData: () => void,
  onNeedAbsenceData: () => void,
  onWorkChange: (clientId: number, workLog: WorkLogItem) => void,
  onWorkRemove: (clientId: number, workLog: WorkLogItem) => void,
  onChangeEmployee: (employee: number, date: Date) => void,
  onChangeDate: (date: Date) => void,
|}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
  return {
    onNeedEmployeeData: () => {
      dispatch(loadEmployees())
    },
    onNeedWorkAccountData: () => {
      dispatch(loadWorkAccounts())
    },
    onNeedWorkLogData: () => {
      dispatch(loadWorkLogs())
    },
    onNeedScheduleData: () => {
      dispatch(loadSchedule())
    },
    onNeedAbsenceData: () => {
      dispatch(loadAbsence())
    },
    onWorkChange: (clientId: number, workLog: WorkLogItem) => {
      if (workLog.id) {
        dispatch(updateWorkLog(clientId, workLog))
      } else {
        dispatch(createWorkLog(clientId, workLog))
      }
    },
    onWorkRemove: (clientId: number, workLog: WorkLogItem) => {
      dispatch(removeWorkLog(clientId, workLog))
    },
    onChangeEmployee: (employee: number, date: Date) => {
      dispatch(changeEmployee(employee))
      dispatch(changeDate(date))
    },
    onChangeDate: (date: Date) => {
      dispatch(changeDate(date))
    },
  }
}

function mergeProps(stateProps: StateProps, dispatchProps: DispatchProps): WorkLogEditProps {
  return {
    className: stateProps.className,
    employee: stateProps.employee,
    date: stateProps.date,
    workLog: stateProps.workLog,
    scheduleHours: stateProps.scheduleHours,
    absence: stateProps.absence,
    employees: stateProps.employees,
    workAccounts: stateProps.workAccounts,
    onNeedEmployeeData: dispatchProps.onNeedEmployeeData,
    onNeedWorkAccountData: dispatchProps.onNeedWorkAccountData,
    onNeedWorkLogData: dispatchProps.onNeedWorkLogData,
    onNeedScheduleData: dispatchProps.onNeedScheduleData,
    onNeedAbsenceData: dispatchProps.onNeedAbsenceData,
    onWorkChange: dispatchProps.onWorkChange,
    onWorkRemove: dispatchProps.onWorkRemove,
    onChangeEmployee: (employee: number) => {
      let date = new Date()
      if (stateProps.workLogs) {
        const dates = stateProps.workLogs
          .filter((workLog: WorkLogState) => workLog.data.employee === employee)
          .map((workLog: WorkLogState) => workLog.data.date)
          .sort()

        if (dates.length > 0) {
          date = new Date(dates[dates.length - 1])
        }
      }
      dispatchProps.onChangeEmployee(employee, date)
    },
    onChangeDate: dispatchProps.onChangeDate,
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WorkLogEdit)
