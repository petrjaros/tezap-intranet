// @flow
import PageLayout from '../../layouts/PageLayout'
import WorkLogEdit from './WorkLogEdit'

export default PageLayout(WorkLogEdit)
