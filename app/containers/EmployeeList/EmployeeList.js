// @flow
import { connect } from 'react-redux'
import EmployeeList from '../../components/EmployeeList'
import {
  loadEmployees,
  createEmployee,
  updateEmployee,
  removeEmployee,
} from '../../actions/employeeActions'
import { loadWorkAccounts } from '../../actions/workAccountActions'
import { getWorkAccountsSelector } from '../../selectors/workAccountSelectors'
import type { Dispatch, State, Employee, WorkAccounts } from '../../types'
import type { EmployeeListProps } from '../../components/EmployeeList'
import type { EmployeeState } from '../../reducers/employeeReducer'

type StateProps = {|
  data: ?Array<EmployeeState>,
  workAccounts: ?WorkAccounts,
|}

function mapStateToProps(state: State): StateProps {
  return {
    data: state.employee.data,
    workAccounts: getWorkAccountsSelector(state),
  }
}

type DispatchProps = {|
  onNeedEmployeeData: () => void,
  onNeedWorkAccountData: () => void,
  onChange: (number, Employee) => void,
  onRemove: (number, Employee) => void,
|}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
  return {
    onNeedEmployeeData: () => {
      dispatch(loadEmployees())
    },
    onNeedWorkAccountData: () => {
      dispatch(loadWorkAccounts())
    },
    onChange: (clientId: number, employee: Employee) => {
      if (employee.id) {
        dispatch(updateEmployee(clientId, employee))
      } else {
        dispatch(createEmployee(clientId, employee))
      }
    },
    onRemove: (clientId: number, employee: Employee) => {
      dispatch(removeEmployee(clientId, employee))
    },
  }
}

function mergeProps(stateProps: StateProps, dispatchProps: DispatchProps): EmployeeListProps {
  return {
    data: stateProps.data,
    workAccounts: stateProps.workAccounts,
    onNeedEmployeeData: dispatchProps.onNeedEmployeeData,
    onNeedWorkAccountData: dispatchProps.onNeedWorkAccountData,
    onChange: dispatchProps.onChange,
    onRemove: dispatchProps.onRemove,
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(EmployeeList)
