// @flow
import PageLayout from '../../layouts/PageLayout'
import EmployeeList from './EmployeeList'

export default PageLayout(EmployeeList)
