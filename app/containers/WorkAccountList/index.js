// @flow
import PageLayout from '../../layouts/PageLayout'
import WorkAccountList from './WorkAccountList'

export default PageLayout(WorkAccountList)
