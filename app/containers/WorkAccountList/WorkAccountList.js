// @flow
import { connect } from 'react-redux'
import WorkAccountList from '../../components/WorkAccountList'
import {
  loadWorkAccounts,
  createWorkAccount,
  updateWorkAccount,
  removeWorkAccount,
} from '../../actions/workAccountActions'
import type { Dispatch, State, WorkAccount } from '../../types'
import type { WorkAccountListProps } from '../../components/WorkAccountList'
import type { WorkAccountState } from '../../reducers/workAccountReducer'

type StateProps = {|
  data: ?Array<WorkAccountState>,
|}

function mapStateToProps(state: State): StateProps {
  return {
    data: state.workAccount.data,
  }
}

type DispatchProps = {|
  onNeedWorkAccountData: () => void,
  onChange: (number, WorkAccount) => void,
  onRemove: (number, WorkAccount) => void,
|}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
  return {
    onNeedWorkAccountData: () => {
      dispatch(loadWorkAccounts())
    },
    onChange: (clientId: number, workAccount: WorkAccount) => {
      if (workAccount.id) {
        dispatch(updateWorkAccount(clientId, workAccount))
      } else {
        dispatch(createWorkAccount(clientId, workAccount))
      }
    },
    onRemove: (clientId: number, workAccount: WorkAccount) => {
      dispatch(removeWorkAccount(clientId, workAccount))
    },
  }
}

function mergeProps(stateProps: StateProps, dispatchProps: DispatchProps): WorkAccountListProps {
  return {
    data: stateProps.data,
    onNeedWorkAccountData: dispatchProps.onNeedWorkAccountData,
    onChange: dispatchProps.onChange,
    onRemove: dispatchProps.onRemove,
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WorkAccountList)
