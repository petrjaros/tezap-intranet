// @flow
import PageLayout from '../../layouts/PageLayout'
import Dashboard from './Dashboard'

export default PageLayout(Dashboard)
