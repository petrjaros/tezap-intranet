// @flow

// Import all the third party stuff
import React from 'react'
import { Route } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'

import Dashboard from '../Dashboard'
import WorkLogEdit from '../WorkLogEdit'
import WorkLogMonthlyReport from '../WorkLogMonthlyReport'
import EmployeeList from '../EmployeeList'
import WorkAccountList from '../WorkAccountList'

// Import CSS reset and Global Styles
// import './global-styles';

type Props = {
  history: any,
}

export default (props: Props) => {
  const { history } = props

  return (
    <ConnectedRouter history={history}>
      <div>
        <Route exact path="/" component={Dashboard} />
        <Route exact path="/zadavani-prace" component={WorkLogEdit} />
        <Route exact path="/tisk-vykazu" component={WorkLogMonthlyReport} />
        <Route exact path="/zamestnanci" component={EmployeeList} />
        <Route exact path="/konta" component={WorkAccountList} />
      </div>
    </ConnectedRouter>
  )
}
