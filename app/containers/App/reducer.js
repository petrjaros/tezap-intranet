// @flow
import {
} from './constants'

type State = {}
type Action = {|
  type: string,
|}

const initialState = {}

function appReducer(state: State = initialState, action: Action) {
  switch (action.type) {
    default:
      return state
  }
}

export default appReducer
