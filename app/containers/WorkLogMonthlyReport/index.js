// @flow
import PageLayout from '../../layouts/PageLayout'
import WorkLogMonthlyReport from './WorkLogMonthlyReport'

export default PageLayout(WorkLogMonthlyReport)
