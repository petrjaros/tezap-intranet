// @flow
import { connect } from 'react-redux'
import WorkLogMonthlyReport from '../../components/WorkLogMonthlyReport'
import { loadEmployees } from '../../actions/employeeActions'
import { loadWorkLogs } from '../../actions/workLogActions'
import { loadSchedule } from '../../actions/scheduleActions'
import { loadAbsence } from '../../actions/absenceActions'
import { changeMonthlyReportMonth } from '../../actions/workLogMonthlyReportActions'
import { getEmployeesSelector } from '../../selectors/employeeSelectors'
import type { Dispatch, State, WorkLogState, Employees, AbsenceItem } from '../../types'
import type { Props as WorkLogMonthlyReportProps } from '../../components/WorkLogMonthlyReport'

type Props = {|
  className: string,
|}

type StateProps = {|
  employees: Employees,
  month: number,
  year: number,
  workLog: Array<WorkLogState>,
  scheduleHours: { [number]: { [string]: number } },
  absence: { [number]: { [string]: Array<AbsenceItem> }},
|}

function mapStateToProps(state: State): StateProps {
  return {
    employees: getEmployeesSelector(state),
    month: state.workLogMonthlyReport.month,
    year: state.workLogMonthlyReport.year,
    workLog: state.workLog.data,
    scheduleHours: state.schedule.data || {},
    absence: state.absence.data || {},
  }
}

export type DispatchProps = {|
  onNeedEmployeeData: () => void,
  onNeedWorkLogData: () => void,
  onMonthChange: (year: number, month: number) => void,
  onNeedScheduleData: () => void,
  onNeedAbsenceData: () => void,
|}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
  return {
    onNeedEmployeeData: () => {
      dispatch(loadEmployees())
    },
    onNeedWorkLogData: () => {
      dispatch(loadWorkLogs())
    },
    onMonthChange: (year: number, month: number) => {
      dispatch(changeMonthlyReportMonth(year, month))
    },
    onNeedScheduleData: () => {
      dispatch(loadSchedule())
    },
    onNeedAbsenceData: () => {
      dispatch(loadAbsence())
    },
  }
}

function mergeProps(
  stateProps: StateProps,
  dispatchProps: DispatchProps,
  ownProps: Props
): WorkLogMonthlyReportProps {
  return {
    className: ownProps.className,
    employees: stateProps.employees,
    month: stateProps.month,
    year: stateProps.year,
    workLog: stateProps.workLog,
    scheduleHours: stateProps.scheduleHours,
    absence: stateProps.absence,
    onNeedEmployeeData: dispatchProps.onNeedEmployeeData,
    onNeedWorkLogData: dispatchProps.onNeedWorkLogData,
    onMonthChange: dispatchProps.onMonthChange,
    onNeedScheduleData: dispatchProps.onNeedScheduleData,
    onNeedAbsenceData: dispatchProps.onNeedAbsenceData,
  }
}

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(WorkLogMonthlyReport)
