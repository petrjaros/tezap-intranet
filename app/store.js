// @flow
import { createStore, applyMiddleware, compose } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'
import createReducer from './reducers'
import sagas from './sagas'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore(initialState: {} = {}, history: any) {
  const middlewares = [
    sagaMiddleware,
    routerMiddleware(history),
  ]

  const enhancers = [
    applyMiddleware(...middlewares),
  ]

  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = process.env.NODE_ENV !== 'production'
    && typeof window === 'object'
    && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose
  /* eslint-enable */

  const store = createStore(
    connectRouter(history)(createReducer()),
    initialState,
    composeEnhancers(...enhancers)
  )

  sagas.map(sagaMiddleware.run)

  // Extensions
  // store.runSaga = sagaMiddleware.run
  // store.asyncReducers = {}

  return store
}
