// @flow
import type { Action } from './actionTypes'
import type { Employee } from '../types'

export function loadEmployees(): Action {
  return {
    type: 'START_LOAD_EMPLOYEES',
  }
}

export function createEmployee(clientId: number, employee: Employee): Action {
  return {
    type: 'START_CREATE_EMPLOYEE',
    clientId,
    employee,
  }
}

export function updateEmployee(clientId: number, employee: Employee): Action {
  return {
    type: 'START_UPDATE_EMPLOYEE',
    clientId,
    employee,
  }
}

export function removeEmployee(clientId: number, employee: Employee): Action {
  return {
    type: 'START_REMOVE_EMPLOYEE',
    clientId,
    employee,
  }
}
