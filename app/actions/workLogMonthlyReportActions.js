// @flow
import type { Action } from './actionTypes'

export function changeMonthlyReportMonth(year: number, month: number): Action {
  return {
    type: 'CHANGE_MONTHLY_REPORT_MONTH',
    month,
    year,
  }
}
