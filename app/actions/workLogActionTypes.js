// @flow
import type { WorkLog, WorkLogItem } from '../types'

export type ChangeEmployee = {
  type: 'CHANGE_EMPLOYEE',
  employee: number,
}

export type ChangeDate = {
  type: 'CHANGE_DATE',
  date: Date,
}

export type StartLoadWorkLogsAction = {
  type: 'START_LOAD_WORK_LOGS',
}

export type FinishLoadWorkLogsAction = {
  type: 'FINISH_LOAD_WORK_LOGS',
  workLogs: ?WorkLog,
}

export type StartCreateWorkLogAction = {
  type: 'START_CREATE_WORK_LOG',
  clientId: number,
  workLog: WorkLogItem,
}

export type FinishCreateWorkLogAction = {
  type: 'FINISH_CREATE_WORK_LOG',
  clientId: number,
  serverId: ?number,
  workLog: WorkLogItem,
}

export type StartUpdateWorkLogAction = {
  type: 'START_UPDATE_WORK_LOG',
  clientId: number,
  workLog: WorkLogItem,
}

export type FinishUpdateWorkLogAction = {
  type: 'FINISH_UPDATE_WORK_LOG',
  clientId: number,
  success: boolean,
}

export type StartRemoveWorkLogAction = {
  type: 'START_REMOVE_WORK_LOG',
  clientId: number,
  workLog: WorkLogItem,
}

export type FinishRemoveWorkLogAction = {
  type: 'FINISH_REMOVE_WORK_LOG',
  success: boolean,
  clientId: number,
}

export type Action =
  | ChangeEmployee
  | ChangeDate
  | StartLoadWorkLogsAction
  | FinishLoadWorkLogsAction
  | StartCreateWorkLogAction
  | FinishCreateWorkLogAction
  | StartUpdateWorkLogAction
  | FinishUpdateWorkLogAction
  | StartRemoveWorkLogAction
  | FinishRemoveWorkLogAction
