// @flow
import type { Action } from './actionTypes'

export function loadAbsence(): Action {
  return {
    type: 'START_LOAD_ABSENCE',
  }
}
