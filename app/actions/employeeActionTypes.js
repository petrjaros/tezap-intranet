// @flow
import type { Employees, Employee } from '../types'

export type StartLoadEmployeesAction = {
  type: 'START_LOAD_EMPLOYEES',
}

export type FinishLoadEmployeesAction = {
  type: 'FINISH_LOAD_EMPLOYEES',
  employees: ?Employees,
}

export type StartCreateEmployeeAction = {
  type: 'START_CREATE_EMPLOYEE',
  clientId: number,
  employee: Employee,
}

export type FinishCreateEmployeeAction = {
  type: 'FINISH_CREATE_EMPLOYEE',
  clientId: number,
  serverId: ?number,
  employee: Employee,
}

export type StartUpdateEmployeeAction = {
  type: 'START_UPDATE_EMPLOYEE',
  clientId: number,
  employee: Employee,
}

export type FinishUpdateEmployeeAction = {
  type: 'FINISH_UPDATE_EMPLOYEE',
  clientId: number,
  success: boolean,
}

export type StartRemoveEmployeeAction = {
  type: 'START_REMOVE_EMPLOYEE',
  clientId: number,
  employee: Employee,
}

export type FinishRemoveEmployeeAction = {
  type: 'FINISH_REMOVE_EMPLOYEE',
  success: boolean,
  clientId: number,
}

export type Action =
  | StartLoadEmployeesAction
  | FinishLoadEmployeesAction
  | StartCreateEmployeeAction
  | FinishCreateEmployeeAction
  | StartUpdateEmployeeAction
  | FinishUpdateEmployeeAction
  | StartRemoveEmployeeAction
  | FinishRemoveEmployeeAction
