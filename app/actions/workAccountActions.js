// @flow
import type { Action } from './actionTypes'
import type { WorkAccount } from '../types'

export function loadWorkAccounts(): Action {
  return {
    type: 'START_LOAD_WORK_ACCOUNTS',
  }
}

export function createWorkAccount(clientId: number, workAccount: WorkAccount): Action {
  return {
    type: 'START_CREATE_WORK_ACCOUNT',
    clientId,
    workAccount,
  }
}

export function updateWorkAccount(clientId: number, workAccount: WorkAccount): Action {
  return {
    type: 'START_UPDATE_WORK_ACCOUNT',
    clientId,
    workAccount,
  }
}

export function removeWorkAccount(clientId: number, workAccount: WorkAccount): Action {
  return {
    type: 'START_REMOVE_WORK_ACCOUNT',
    clientId,
    workAccount,
  }
}
