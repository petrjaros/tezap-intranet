// @flow
import type { Schedule } from '../types'

export type StartLoadScheduleAction = {
  type: 'START_LOAD_SCHEDULE',
}

export type FinishLoadScheduleAction = {
  type: 'FINISH_LOAD_SCHEDULE',
  schedule: ?Schedule,
}

export type Action =
  | StartLoadScheduleAction
  | FinishLoadScheduleAction
