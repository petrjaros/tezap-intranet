// @flow
import type { WorkAccounts, WorkAccount } from '../types'

export type StartLoadWorkAccountsAction = {
  type: 'START_LOAD_WORK_ACCOUNTS',
}

export type FinishLoadWorkAccountsAction = {
  type: 'FINISH_LOAD_WORK_ACCOUNTS',
  workAccounts: ?WorkAccounts,
}

export type StartCreateWorkAccountAction = {
  type: 'START_CREATE_WORK_ACCOUNT',
  clientId: number,
  workAccount: WorkAccount,
}

export type FinishCreateWorkAccountAction = {
  type: 'FINISH_CREATE_WORK_ACCOUNT',
  clientId: number,
  serverId: ?number,
  workAccount: WorkAccount,
}

export type StartUpdateWorkAccountAction = {
  type: 'START_UPDATE_WORK_ACCOUNT',
  clientId: number,
  workAccount: WorkAccount,
}

export type FinishUpdateWorkAccountAction = {
  type: 'FINISH_UPDATE_WORK_ACCOUNT',
  clientId: number,
  success: boolean,
}

export type StartRemoveWorkAccountAction = {
  type: 'START_REMOVE_WORK_ACCOUNT',
  clientId: number,
  workAccount: WorkAccount,
}

export type FinishRemoveWorkAccountAction = {
  type: 'FINISH_REMOVE_WORK_ACCOUNT',
  success: boolean,
  clientId: number,
}

export type Action =
  | StartLoadWorkAccountsAction
  | FinishLoadWorkAccountsAction
  | StartCreateWorkAccountAction
  | FinishCreateWorkAccountAction
  | StartUpdateWorkAccountAction
  | FinishUpdateWorkAccountAction
  | StartRemoveWorkAccountAction
  | FinishRemoveWorkAccountAction
