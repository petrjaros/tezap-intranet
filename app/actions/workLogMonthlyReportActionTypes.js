// @flow

export type ChangeMonth = {
  type: 'CHANGE_MONTHLY_REPORT_MONTH',
  month: number,
  year: number,
}

export type Action =
  | ChangeMonth
