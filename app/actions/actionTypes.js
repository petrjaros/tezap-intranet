// @flow
import type { Action as WorkLogAction } from './workLogActionTypes'
import type { Action as ScheduleAction } from './scheduleActionTypes'
import type { Action as AbsenceAction } from './absenceActionTypes'
import type { Action as WorkLogMonthlyReportAction } from './workLogMonthlyReportActionTypes'
import type { Action as EmployeeAction } from './employeeActionTypes'
import type { Action as WorkAccountAction } from './workAccountActionTypes'

export type Action =
  | WorkLogAction
  | ScheduleAction
  | AbsenceAction
  | WorkLogMonthlyReportAction
  | EmployeeAction
  | WorkAccountAction
