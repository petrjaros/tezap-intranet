// @flow
import type { Action } from './actionTypes'

export function loadSchedule(): Action {
  return {
    type: 'START_LOAD_SCHEDULE',
  }
}
