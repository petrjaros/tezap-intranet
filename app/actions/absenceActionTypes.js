// @flow
import type { Absence } from '../types'

export type StartLoadAbsenceAction = {
  type: 'START_LOAD_ABSENCE',
}

export type FinishLoadAbsenceAction = {
  type: 'FINISH_LOAD_ABSENCE',
  absence: ?Absence,
}

export type Action =
  | StartLoadAbsenceAction
  | FinishLoadAbsenceAction
