// @flow
import type { Action } from './actionTypes'
import type { WorkLogItem } from '../types'

export function changeEmployee(employee: number): Action {
  return {
    type: 'CHANGE_EMPLOYEE',
    employee,
  }
}

export function changeDate(date: Date): Action {
  return {
    type: 'CHANGE_DATE',
    date,
  }
}

export function loadWorkLogs(): Action {
  return {
    type: 'START_LOAD_WORK_LOGS',
  }
}

export function createWorkLog(clientId: number, workLog: WorkLogItem): Action {
  return {
    type: 'START_CREATE_WORK_LOG',
    clientId,
    workLog,
  }
}

export function updateWorkLog(clientId: number, workLog: WorkLogItem): Action {
  return {
    type: 'START_UPDATE_WORK_LOG',
    clientId,
    workLog,
  }
}

export function removeWorkLog(clientId: number, workLog: WorkLogItem): Action {
  return {
    type: 'START_REMOVE_WORK_LOG',
    clientId,
    workLog,
  }
}
