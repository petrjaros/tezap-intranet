Instalace
=========

Fedora
------

```bash
sudo dnf install git
sudo dnf install nodejs
sudo dnf install gcc-c++
git clone https://petrjaros@bitbucket.org/petrjaros/tezap-intranet.git tezap # stáhne program a uloží do nové složky tezap
cd tezap
npm install --production # stáhne potřebné knihovny
mkdir import # je potřeba vytvořit složku, kam se poté budou ukládat datové soubory z píchaček
npm run build # provede build klientského JavaScriptu
# Poté je potřeba uložit databázi tezap.sqlite (v příloze) do složky data. Databáze v příloze není aktuální, ale na vyzkoušení postačí.
```

Spuštění
========

```bash
npm run start:prod # program spustí HTTP server na portu 3000
```

Update
======

```bash
git checkout -- . # smaže případné úpravy kódu
git pull # stáhne novou verzi
npm install -–production # stáhne potřebné knihovny
npm run build # provede build klientského JavaScriptu
```
